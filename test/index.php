<?php
apc_clear_cache('user');
require dirname(__DIR__) . '/vendor/Turbine/autoload.php';
require dirname(__DIR__) . '/vendor/Smarty/libs/Smarty.class.php';

// $web_turbine = Turbine::getInstance();
// apc_store('turbine.web', $web_turbine);

// $web_turbine = Turbine::getInstance();
// apc_store('turbine.web', $web_turbine);
// $web_turbine = apc_fetch('turbine.web', $web_turbine);


// $x = new ReflectionClass(unserialize(serialize($web_turbine)));
// var_dump($x->getMethods());

TemplateEngine::setEngine(
    'smarty',
    array(
        'template_dir' => __DIR__ . '/template/',
        'compile_dir' => __DIR__ . '/template_compile/',
        'caching' => true,
        'cache_lifetime' => 300
    )
);

$web_turbine = null;

$apc = Cache::engine('apc');

$config_path = __DIR__ . '/config.php';

require __DIR__ . '/init.php';

if (false === ($web_turbine = $apc->get('turbine.web'))) {
    $web_turbine = Turbine::getInstance();

    CacheCompilerModule::register();
    EventCompilerModule::register();
    RouteCompilerModule::register();
    ErrorRendererCompilerModule::register();

    Compiler::inspectNLoad();
    Compiler::load(array('class' => array('Test')));
    Compiler::compile();
    Configure::load($config_path);
    Turbine\Web\App\App::load(__DIR__ . '/app/');

    $apc->store('turbine.web', $web_turbine);
    $apc->store('turbine.configure', Configure::getInstance());
}

$apc->get('turbine.configure');

$web_turbine->bootstrap();
