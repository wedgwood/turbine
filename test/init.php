<?php
// $web_turbine->register('init', function() {
    // echo 'init';
// })->register('uninit', function(){
    // echo 'uninit';
// });

// route('/age/<age:int>/', HttpMethod::GET, function($age) {
    // return $age . ' years old ?';
// });

/**
 * abc
 *
 * @when(oninit)
 * @when(oninit)
 * @return void
 */
function abc()
{
}

/**
 * hello
 *
 * @get(/hello/<name>)
 * @param mixed $name
 * @param mixed $rqst
 * @access public
 * @return void
 */
function hello($name, $rqst)
{
    return date('Y-m-d H:i:s', $rqst->getRequestTime()) . ': Hi ' . $name . '!';
}

/**
 * a
 * @when(onrequest)
 */
function a()
{
}

/**
 * user_name
 *
 * @param mixed $name
 * @ get ( /user/<name>/ )
 * @access public
 * @return void
 */
function user($name)
{
    // use path/you/put/index.php/user/[your name]/
    $prefix = Configure::select('a.aa.aaa1');
    return $prefix . $name;
    return template('hello_world.tpl', array('name' => $prefix . $name));
    if (templateCached('hello_world.tpl')) {
        return template('hello_world.tpl');
    } else {
        return template('hello_world.tpl', array('name' => $name));
    }

    return 'hello ' . Cookie::get('name')
        . ' your passwd: ' . Cookie::get('passwd') . '!';
}

// get(
    // '/mail/<from>/<to>/',
    // function($from, $to) {
        // return 'mail from: ' . $from . ' to ' . $to;
    // }
// );

/**
 * Test
 * @magic(on)
 * @prefix(/test/)
 */
class Test
{
    /**
     * @get(/confirm/<abc:int>)
     */
    public function alert($abc)
    {
        return $abc;
    }

    public function haha()
    {
        return 'confirm';
    }
}

// $web_turbine->addRoutable('/test/', 'Test');

// error(function($errno, $errstr, $errfile, $errline, $errcontext) {
    // printf(
        // '[%s:%d][%d:%s][%s]',
        // basename($errfile),
        // $errline,
        // $errno,
        // $errstr,
        // print_r($errcontext, true)
    // );
// }, null);

/**
 * go
 *
 * @get(/go/<name>/)
 * @access public
 * @return void
 */
function go($name)
{
    return "go {$name}";
}

/**
 * status404
 *
 * @ error( 404 )
 * @access public
 * @return void
 */
function status404($resp)
{
    return '404';
}
