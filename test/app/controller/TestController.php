<?php
/**
 * TestController
 *
 * @preifx(/test/)
 * @package placeholder
 * @version 1.0.0
 * @copyright Copyright 2011 by Your Name <the_guy_1987@hotmail.com>
 * @author B.J. Yuan <the_guy_1987@hotmail.com>
 * @license Provided under the GPL (http://www.gnu.org/copyleft/gpl.html)
 */
class TestController
{
    public function __construct()
    {
    }

    /**
     * test
     * @get(/haode/<name>/)
     * @return void
     */
    public function test($name) {
        return 'hello ' . $name;;
    }
}
