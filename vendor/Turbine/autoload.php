<?php
require __DIR__ . '/ClassLoader/ClassLoader.php';
require __DIR__ . '/ClassLoader/ApcClassLoader.php';

$loader = new ApcClassLoader('turbine.loader', 'Turbine', dirname(__DIR__));
// $loader = new ClassLoader('Turbine', dirname(__DIR__));
$loader->register();

require __DIR__ . '/export.php';

\Turbine\export('web');
