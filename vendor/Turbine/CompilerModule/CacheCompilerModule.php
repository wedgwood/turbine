<?php
namespace Turbine\CompilerModule;

use Turbine\Compiler\InvokerBag;
use Turbine\Compiler\CompileStage;
use Turbine\CompilerModule\AbstractCompilerModule;
use Turbine\Cache\Cache;
use Turbine\InvokerWrapper\CacheInvokerWrapper;

class CacheCompilerModule extends AbstractCompilerModule
{
    private static $regex_ = <<<'EOF'
#\s*(?P<ttl>\d+)\s*,\s*(?P<engine>.+(?<!\s))?#
EOF;

    private static $default_cache_engine_ = '';

    const COMPILE_STAGE = CompileStage::PREPARE;

    public static function setDefaultCacheEngine($engine)
    {
        self::$default_cache_engine_ = $engine;
    }

    public function compile(InvokerBag $invoker_bag)
    {
        $turbine = \Turbine::getInstance();
        $events = \Event::getConstants();

        foreach ($invoker_bag as $key => $entry) {
            $annotation = $entry->annotation;
            $invoker = $entry->invoker;

            if ($annotation->has('cache')) {
                $config = $annotation->get('cache');
                $match_cnt = preg_match(self::$regex_, $config[0], $match);

                $ttl = 0;
                $engine = null;

                if ($match_cnt) {
                    $ttl = $match['ttl'];
                    $engine = empty($match['engine']) ?
                        self::$default_cache_engine_ : $match['engine'];
                } else {
                    $engine = self::$default_cache_engine_;
                }

                $entry->invoker = new CacheInvokerWrapper(
                                        $key, $invoker, $engine, $ttl);
            }
        }
    }
}
