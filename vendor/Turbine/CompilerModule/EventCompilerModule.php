<?php
namespace Turbine\CompilerModule;

use Turbine\Compiler\InvokerBag;
use Turbine\Compiler\CompileStage;
use Turbine\CompilerModule\AbstractCompilerModule;

class EventCompilerModule extends AbstractCompilerModule
{
    const COMPILE_STAGE = CompileStage::COMPILE;

    public function compile(InvokerBag $invoker_bag)
    {
        $turbine = \Turbine::getInstance();
        $events = \Event::getConstants();

        foreach ($invoker_bag as $entry) {
            $annotation = $entry->annotation;
            $invoker = $entry->invoker;

            if ($annotation->has('when')) {
                foreach ($annotation->get('when') as $when) {
                    $turbine->register($when, $invoker);
                }
            }
        }
    }
}
