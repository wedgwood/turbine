<?php
namespace Turbine\CompilerModule;

use Turbine\Compiler\Compiler;
use Turbine\Compiler\InvokerBag;

abstract class AbstractCompilerModule
{
    public static function register()
    {
        Compiler::registerModule(new static());
    }

    abstract public function compile(InvokerBag $invoker_bag);

    public function getCompileStage()
    {
        return static::COMPILE_STAGE;
    }
}
