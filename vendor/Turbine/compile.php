<?php

$files = array();

function fillFiles($dir)
{
    $dh = opendir($dir);

    if (!$dh) {
        exit('error');
    }

    global $files;

    while ($file = readdir($dh)) {
        if ($file[0] == '.') {
            continue;
        }

        $fullpath = $dir . '/' . $file;

        if (is_dir($fullpath)) {
            fillFiles($fullpath);
            continue;
        }

        if (substr($fullpath, -4) == '.php') {
            $files[] = $fullpath;
        }
    }
}

fillFiles(__DIR__);
apc_bin_dumpfile($files, null, './a.c');
apc_bin_loadfile('./a.c');

$turbine = new Turbine\Core\Turbine();
