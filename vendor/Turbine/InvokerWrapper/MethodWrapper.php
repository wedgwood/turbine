<?php
namespace Turbine\InvokerWrapper;

class MethodWrapper extends AbstractInvokerWrapper
{
    private $method_;
    private $name_;
    private $class_;
    private $filename_;

    public function __construct($class, $method, $filename)
    {
        $this->name_ = $this->method_ = $method;
        $this->class_ = $class;
        $this->filename_ = $filename;
    }

    public function getName()
    {
        return $this->name_;
    }

    public function call()
    {
        if (!class_exists($this->class_)) {
            require $this->filename_;
        }

        return call_user_func_array(
            array(new $this->class_, $this->method_),
            func_get_args()
        );
    }
}
