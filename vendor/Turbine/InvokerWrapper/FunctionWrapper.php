<?php
namespace Turbine\InvokerWrapper;

class FunctionWrapper extends AbstractInvokerWrapper
{
    private $invoker_;
    private $name_;
    private $filename_;

    public function __construct($function, $filename)
    {
        $this->filename_ = $filename;
        $this->name_ = $this->invoker_ = $function;
    }

    public function getName()
    {
        return $this->name_;
    }

    public function call()
    {
        if (!function_exists($this->invoker_)) {
            require $this->filename_;
        }

        return call_user_func_array($this->invoker_, func_get_args());
    }
}
