<?php
namespace Turbine\InvokerWrapper;

abstract class AbstractInvokerWrapper
{
    abstract public function getName();
    abstract public function call();

    public function __invoke()
    {
        return call_user_func_array(array($this, 'call'), func_get_args());
    }
}
