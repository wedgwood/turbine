<?php
namespace Turbine\InvokerWrapper;

use Turbine\Cache\Cache;

class CacheInvokerWrapper
{
    private $cache_engine_type_;
    private $invoker_;
    private $ttl_;
    private $key_;

    public function __construct($key, $invoker, $engine = null, $ttl = 0)
    {
        $this->cache_engine_type_ = $engine;
        $this->invoker_ = $invoker;
        $this->ttl_ = $ttl;
        $this->key_ = $key;
    }

    public function __invoke()
    {
        $engine = Cache::engine($this->cache_engine_type_);
        $ret = $engine->get($this->key_);

        if (false === $ret) {
            $ret = call_user_func_array($this->invoker_, func_get_args());
            $engine->store($this->key_, $ret, $this->ttl_);
        }

        return $ret;
    }
}
