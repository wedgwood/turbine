<?php
namespace Turbine\Web\Template;

interface ITemplate
{
    public function assign($key, $value);

    public function config($key, $value);

    public function fetch($tpl_name);

    public function display($tpl_name);

    public function isCached($tpl_name);

    public function cache($lifetime);
}
