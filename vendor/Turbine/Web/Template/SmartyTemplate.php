<?php
namespace Turbine\Web\Template;

class SmartyTemplate implements ITemplate
{
    private $smarty_tempalte_;

    public function __construct($config = null)
    {
        $this->smarty_tempalte_ = new \Smarty();

        if (isset($config)) {
            foreach ($config as $key => $value) {
                $this->smarty_tempalte_->{$key} = $value;
            }
        }
    }

    public function assign($key_or_value, $value = null)
    {
        if (is_array($key_or_value)) {
            $this->smarty_tempalte_->assign($key_or_value);
        } else {
            $this->smarty_tempalte_->assign($key_or_value, $value);
        }

        return $this;
    }

    public function fetch($tpl_name, $values = null)
    {
        if (isset($values)) {
            $this->assign($values);
        }

        return $this->smarty_tempalte_->fetch($tpl_name);
    }

    public function display($tpl_name, $values = null)
    {
        if (isset($values)) {
            $this->assign($values);
        }

        return $this;
    }

    public function cache($lifetime)
    {
        $this->smarty_tempalte_->setCacheLifetime($lifetime);

        return $this;
    }

    public function isCached($tpl_name)
    {
        return $this->smarty_tempalte_->isCached($tpl_name);
    }

    public function config($key_or_value, $value = null)
    {
        if (is_array($key_or_value)) {
            foreach ($key_or_value as $key => $value) {
                $this->smarty_tempalte_->{$key} = $value;
            }
        } else {
            $this->smarty_tempalte_->{$key_or_value} = $value;
        }

        return $this;
    }
}
