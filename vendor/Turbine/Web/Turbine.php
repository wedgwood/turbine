<?php
namespace Turbine\Web;

use Turbine\Core,
    Turbine\Core\Environ,
    Turbine\Web\Constant\HttpStatus,
    Turbine\Web\Event\Event,
    Turbine\Web\Event\EventData,
    Turbine\Web\Router\Route,
    Turbine\Web\Router\Router,
    Turbine\Web\Request\HttpRequest,
    Turbine\Web\Response\HttpResponse,
    Turbine\Web\Response\JSONResponse,
    Turbine\Web\Response\ErrorResponse,
    Turbine\Web\Response\ErrorResponseRenderer;

class Turbine extends Core\Turbine
{
    private $auto_json_;
    private $catch_all_;
    private $router_;
    private $routes_ = array();

    private $error_handler_ = array();

    public function __construct(
        $auto_trailing_slash = true,
        $catch_all = true,
        $auto_json = true
    ) {
        $this->catch_all_ = $catch_all;
        $this->auto_json_ = $auto_json;

        $this->router_ = new Router($auto_trailing_slash);
    }

    public function bootstrap()
    {
        parent::bootstrap();

        $request = new HttpRequest();
        $response = null;
        $data = new EventData();

        try {
            $data->request = $request;
            Environ::set('__request', $request);

            $this->fire(Event::ON_REQUEST, $data);
            $this->fire(Event::BEFORE_ROUTE, $data);

            if (is_null($data->response)) {
                $pair = $this->router_->match($request);

                $this->fire(Event::AFTER_ROUTE, $data);
                $this->fire(Event::BEFORE_HANDLE_REQUEST, $data);

                if (isset($pair) || isset($pair[0])) {
                    $params = null;

                    if ($pair[1]) {
                        $params = $pair[1];
                    }

                    $params[] = $request;
                    $response = call_user_func_array($pair[0], $params);
                } else {
                    notFound();
                }
            }

            if (is_array($response) && $this->auto_json_) {
                $response = new JSONResponse($response);
            } elseif (!($response instanceof HttpResponse)) {
                $response = new HttpResponse($response);
            }
        } catch (ErrorResponse $e) {
            $response = ErrorResponseRenderer::render($e);
        } catch (HttpResponse $e) {
            $response = $e;
        } catch (Exception $e) {
            if (!$this->catch_all_) {
                throw $e;
            }

            $data->exception = $e;

            try {
                $this->fire(Event::ON_ERROR, $data);
            } catch (HttpResponse $e) {
                $response = $e;
            } catch (Exception $e) {
                throw $e;
            }

            if (!isset($response)) {
                $response = $data->response;
            }
        }

        $data->response = $response;

        $this->fire(Event::AFTER_HANDLE_REQUEST, $data);
        $this->output($response);
    }

    public function output($response)
    {
        $headers = Environ::get('__headers', null);

        if (isset($headers)) {
            foreach ($headers as $key => $value) {
                $response->setHeader($key, $value);
            }
        }

        $cookies = Environ::get('__cookies', null);

        if (isset($cookies)) {
            foreach ($cookies as $key => $cookie) {
                $response->setCookie(
                    $key,
                    $cookie['value'],
                    $cookie['expires'],
                    $cookie['path'],
                    $cookie['domain'],
                    $cookie['secure'],
                    $cookie['httponly']
                );
            }
        }

        $response->send();
    }

    public function addRoute(Route $route)
    {
        $this->routes_[] = $route;
        $this->router_->add($route);

        return $this;
    }

    public function addRoutable($path, $routable)
    {
        $routes = $routable::getRoutes();

        foreach ($routes as $entry) {
            if (empty($entry['routes'])) {
                $this->get(
                    $path . $entry['method'] . '/',
                    array($routable, '__r_' . $entry['method'])
                );
            } else {
                foreach ($entry['routes'] as $route) {
                    $this->route(
                        $route['route'],
                        $route['verb'],
                        array(
                            $routable,
                            '__r_' . $entry['method']
                        )
                    );
                }
            }
        }
    }

    public function route(
        $path,
        $method,
        $callable,
        $name = null,
        $config = null)
    {
        if (!is_array($path)) {
            $path = array($path);
        }

        if (!is_array($method)) {
            $method = array($method);
        }

        foreach ($path as $rule) {
            foreach ($method as $verb) {
                $verb = strtoupper($verb);
                $route = new Route($rule, $verb, $callable, $name, $config);
                $this->addRoute($route);
            }
        }

        return $this;
    }

    public function get(
        $path,
        $callable,
        $name = null,
        $config = null)
    {
        return $this->route($path, 'GET', $callable, $name, $config);
    }

    public function post(
        $path,
        $callable,
        $name = null,
        $config = null)
    {
        return $this->route($path, 'POST', $callable, $name, $config);
    }

    public function put(
        $path,
        $callable,
        $name = null,
        $config = null)
    {
        return $this->route($path, 'PUT', $callable, $name, $config);
    }

    public function delete(
        $path,
        $callable,
        $name = null,
        $config = null)
    {
        return $this->route($path, 'DELETE', $callable, $name, $config);
    }

    public function __wakeup()
    {
        self::setInstance_($this);
    }
}
