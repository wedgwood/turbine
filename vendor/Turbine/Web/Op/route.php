<?php
use Turbine\Web\Turbine;

function route($rule, $method, $callable)
{
    Turbine::getInstance()->route($rule, $method, $callable);
}

function get($rule, $callable)
{
    Turbine::getInstance()->get($rule, $callable);
}

function put($rule, $callable)
{
    Turbine::getInstance()->put($rule, $callable);
}

function post($rule, $callable)
{
    Turbine::getInstance()->post($rule, $callable);
}

function delete($rule, $callable)
{
    Turbine::getInstance()->delete($rule, $callable);
}

function error($callable)
{
    Turbine::getInstance()->error($callable);
}
