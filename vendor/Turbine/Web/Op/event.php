<?php
use Turbine\Web\Turbine;

function onRequest($callable)
{
    return Turbine::getInstance()->register(Event::ON_REQUEST, $callable);
}

function beforeRoute($callable)
{
    return Turbine::getInstance()->register(Event::BEFORE_ROUTE, $callable);
}

function afterRoute($callable)
{
    return Turbine::getInstance()->register(Event::AFTER_ROUTE, $callable);
}

function beforeHandleRequest($callable)
{
    return Turbine::getInstance()->register(
        Event::BEFORE_HANDLE_REQUEST,
        $callable
    );
}

function afterHandleRequest($callable)
{
    return Turbine::getInstance()->register(
        Event::AFTER_HANDLE_REQUEST,
        $callable
    );
}

function when($when, $callable)
{
    return Turbine::getInstance()->register($when, $callable);
}
