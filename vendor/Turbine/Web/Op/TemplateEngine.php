<?php
namespace Turbine\Web\Op;

class TemplateEngine
{
    private static $engine_;
    private static $config_ = null;

    public static function setEngine($engine, $config = null)
    {
        self::$engine_ = $engine;

        if (isset($config)) {
            self::$config_ = $config;
        }
    }

    public static function config($config)
    {
        self::getInstance()->config($config);
    }

    public static function getInstance()
    {
        $engine = self::$engine_;

        if (!isset($engine)) {
            throw new LogicException();
        }

        static $instance;

        if (!isset($instance)) {
            $engine_class = '\\Turbine\\Web\\Template\\'
                . ucfirst(strtolower($engine)) . 'Template';
            $instance = new $engine_class;

            if (isset(self::$config_)) {
                $instance->config(self::$config_);
            }
        }

        return $instance;
    }
}
