<?php
namespace Turbine\Web\Op;

use Turbine\Core\Environ,
    Turbine\Web\Request\HttpRequest;

class Cookie
{
    public static function get($name = null, $default = null)
    {
        $cookies = Environ::get('__cookies', null);

        if (is_null($name)) {
            return $cookies;
        }

        if (isset($cookies) && isset($cookies[$name])) {
            return $cookies[$name];
        }

        return $default;
    }

    public static function set(
        $name,
        $value = null,
        $expires = null,
        $path = null,
        $domain = null,
        $secure = false,
        $httponly = false)
    {
        $cookies = Environ::get('__cookies', array());
        $cookies[$name] = array(
            'value'     => $value,
            'expires'   => $expires,
            'path'      => $path,
            'domain'    => $domain,
            'secure'    => $secure,
            'httponly'  => $httponly
        );

        Environ::set('__cookies', $cookies);
    }

    public static function has($name)
    {
        $cookies = Environ::get('__cookies', null);

        return !empty($cookies) && isset($cookies[$name]);
    }

    // TODO: expire the cookie
    public static function expire($name)
    {

    }
}
