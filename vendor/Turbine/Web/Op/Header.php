<?php
namespace Turbine\Web\Op;

use Turbine\Core\Environ;

class Header
{
    public static function get($name = null, $default = null)
    {
        $headers = Environ::get('__headers', null);

        if (is_null($name)) {
            return $headers;
        }

        if (isset($headers) && isset($headers[$name])) {
            return $headers;
        }

        return $default;
    }

    public static function set($name, $value)
    {
        $headers = Environ::get('__headers', array());
        $headers[$name] = $value;

        Environ::set('__headers', $headers);
    }

    public static function has($name)
    {
        $headers = Environ::get('__headers', null);

        return !empty($headers) && isset($headers[$name]);
    }
}
