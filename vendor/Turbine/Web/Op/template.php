<?php
function template($tpl_name, $values = null)
{
    $te = \TemplateEngine::getInstance();

    if (isset($values)) {
        $te->assign($values);
    }

    return $te->fetch($tpl_name);
}

function templateCached($tpl_name)
{
    $te = \TemplateEngine::getInstance();

    return $te->isCached($tpl_name);
}
