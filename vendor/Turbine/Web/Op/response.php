<?php
use \Turbine\Web\Request\HttpResquest,
    \Turbine\Web\Response\HttpResponse,
    \Turbine\Web\Response\ErrorResponse,
    \Turbine\Web\Constant\HttpVersion;

function redirect($url, $status = null)
{
    if (!$status) {
        $request = HttpRequest::getInstance();
        $status = 'HTTP/' . HttpVersion::VERSION_11
                    == $request->getServer('SERVER_PROTOCOL') ? 303 : 302;
    }

    throw new HttpResponse('', $status, array('Location' => $url));
}

function notFound($text = '', $status = 404)
{
    throw new ErrorResponse($status, $text);
}

function notModified()
{
    throw new HttpResponse('', 304);
}

function abort($text = 'Unknown Error: Application Stopped.', $status = 500)
{
    throw new ErrorResponse($status, $text);
}

function internalServerError($text = '', $status = 500)
{
    throw new ErrorResponse($status, $text);
}
