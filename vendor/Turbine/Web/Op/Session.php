<?php
namespace Turbine\Web\Op;

class Session
{
    private static $is_started_ = false;
    private static $is_destroyed_ = false;
    private static $is_write_closed_ = false;

    private static $session_handler_ = null;

    private static $session_ = null;
    private static $session_id_ = null;

    public static function start($is_started = false)
    {
        if (self::$is_started_ && self::$is_destroyed_) {
            throw new LogicException(
                'session has already started and destroyed'
            );
        }

        if (!self::$is_started_) {
            session_start();

            self::$is_started_ = true;
        }
    }

    public static function isStarted()
    {
        return self::$is_started_;
    }

    public static function distory()
    {
        if (self::$is_destroyed_) {
            return;
        }

        session_destroy();

        self::$is_destroyed_ = true;
    }

    public static function isDestroyed()
    {
        return self::$is_destroyed_;
    }

    public static function setID($id)
    {
        if (!is_string($id) || empty($id)) {
            throw new InvalidArgumentException(
                'invalid parameter $id, session id must be typeof string'
            );
        }

        session_id($id);
    }

    public static function getID()
    {
        return session_id();
    }

    public static function setSessionHandler(SessionHandlerInterface $handler)
    {
        session_set_save_handler(
            array($handler, 'open'),
            array($handler, 'close'),
            array($handler, 'read'),
            array($handler, 'write'),
            array($handler, 'destroy'),
            array($handler, 'gc')
        );

        self::$session_handler_ = $handler;
    }

    public static function getSessionHandler()
    {
        return self::$session_handler_;
    }

    public static function writeClose()
    {
        if (self::$is_write_closed_) {
            return;
        }

        session_write_close();

        self::$is_write_closed_ = false;
    }

    public static function isWritable()
    {
        return self::$is_write_closed_;
    }

    public static function exists()
    {
        $name = session_name();

        if (1 == ini_get('session.use_cookies') && isset($_COOKIE[$name])) {
            return true;
        } elseif (!empty($_REQUEST[$name])) {
            return true;
        }

        return false;
    }

    public static function setCookieLifeTime($seconds = 0)
    {
        $cookie_params = session_get_cookie_params();

        session_set_cookie_params(
            $seconds,
            $cookie_params['path'],
            $cookie_params['domain'],
            $cookie_params['secure'],
            $cookie_params['httponly']
        );
    }

    public static function expireSessionCookie()
    {
        self::setCookieLifeTime(0);
    }

    public static function has($key)
    {
        return isset($_SESSION[$key]);
    }

    public static function get($key, $default = null)
    {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : $default;
    }

    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public static function getIterator()
    {
        return $_SESSION;
    }

    public static function mock($session)
    {
        $_SESSION = $session;
    }
}
