<?php
use Turbine\Core\Annotation;
use Turbine\Web\Constant\HttpMethod;

function compileAutoRoutes()
{
    $funs = get_defined_functions();
    $funs = $funs['user'];
    $verbs = HttpMethod::getConstants();

    foreach ($funs as $fun) {
        $reflection_fun = new ReflectionFunction($fun);
        $annotation = new Annotation($reflection_fun->getDocComment());

        foreach ($verbs as $verb) {
            $lowerVerb = strtolower($verb);

            if ($annotation->has($lowerVerb)) {
                route($annotation->get($lowerVerb), $verb, $fun);
            }
        }
    }
}
