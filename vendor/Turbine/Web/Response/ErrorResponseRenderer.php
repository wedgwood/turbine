<?php
namespace Turbine\Web\Response;

class ErrorResponseRenderer
{
    private static $renderers_ = array();

    public static function registerRenderer($status, $renderer)
    {
        if (isset(self::$renderers_[$status])) {
            self::$renderers_[$status][] = $renderer;
        } else {
            self::$renderers_[$status] = array($renderer);
        }
    }

    public static function render(ErrorResponse $response)
    {
        $status = $response->getStatus();

        if (isset(self::$renderers_[$status])) {
            foreach (self::$renderers_[$status] as $render) {
                $resp = $render($response);

                if ($resp instanceof HttpResponse) {
                    $response = $resp;
                } elseif (!is_null($resp)) {
                    $response = new HttpResponse((string) $resp);
                }
            }
        }

        return $response;
    }
}
