<?php
namespace Turbine\Web\Response;

class HttpHeaders extends \Turbine\Core\DictBag
{
    private $has_sent_ = false;

    public function __construct($headers = null)
    {
        parent::__construct($headers);
    }

    public function __tostring()
    {
        $ret = '';

        foreach ($this as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $s_value) {
                    $ret .= $key . ':' . $s_value . "\r\n";
                }
            } else {
                $ret .= $key . ':' . $value . "\r\n";
            }
        }

        return $ret;
    }
}
