<?php
namespace Turbine\Web\Response;

class ErrorResponse extends HttpResponse
{
    private $exception_;
    private $traceback_;

    public function __construct(
        $status = 500,
        $content = '',
        $exception = null,
        $traceback = null,
        $headers = null)
    {
        parent::__construct($content, $status, $headers);

        $this->traceback_ = $traceback;
        $this->exception_ = $exception;
    }

    public function __tostring()
    {
        // TODO: some control given to user
        return $this->getStatus();
    }
}
