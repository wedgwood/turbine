<?php
namespace Turbine\Web\Response;

use Turbine\Web\Constant\HttpStatus,
    Turbine\Web\Constant\HttpVersion;

class HttpResponse extends \Exception
{
    private $content_;
    private $status_;
    private $reason_;
    private $headers_;
    private $cookies_;
    private $version_ = HttpVersion::VERSION_10;

    private $headers_has_sent_ = false;
    private $content_has_sent_ = false;

    private $default_content_type_ = 'text/html; charset=utf-8';

    public static function getInstance()
    {
        static $instance;

        if (isset($instance)) {
            return $instance;
        }

        return ($instance = new static());
    }

    public function __construct($content = '', $status = 200, $headers = null)
    {
        if (is_array($headers)) {
            $headers = array_merge(
                array('Content-Type' => $this->default_content_type_),
                $headers
            );
        }

        $this->headers_ = new HttpHeaders($headers);
        $this->cookies_ = new Cookies();
        $this->status_ = $status;
        $this->setContent($content);
    }

    public function getContent()
    {
        return $this->content_;
    }

    public function setContent($content)
    {
        $this->content_ = $content;
        $this->setHeader('Content-Length', strlen($this->content_));

        return $this;
    }

    public function setStatus($status, $reason = null)
    {
        $this->status_ = $status;

        if (!is_null($reason)) {
            $this->setReasonPhrase($reason);
        }

        return $this;
    }

    public function setReasonPhrase($reason)
    {
        $this->reason_ = trim($reason);

        return $this;
    }

    public function getReasonPhrase()
    {
        if (is_null($this->reason_)) {
            return HttpStatus::getReasonPhrase($this->status_);
        }

        return $this->reason_;
    }

    public function setVersion($version)
    {
        if ($version != HttpVersion::VERSION_10
            && $version != HttpVersion::VERSION_11
        ) {
            throw new InvalidArgumentException();
        }

        $this->version_ = $version;

        return $this;
    }

    public function getVersion()
    {
        return $this->version_;
    }

    public function renderStatusLine()
    {
        $status_line = sprintf(
            'HTTP/%s %d %s',
            $this->getVersion(),
            $this->getStatus(),
            $this->getReasonPhrase()
        );

        return trim($status_line);
    }

    public function getHeaders()
    {
        return $this->headers_;
    }

    public function setHeader($key, $value)
    {
        $this->headers_->set($key, $value);

        return $this;
    }

    public function removeHeader($key)
    {
        $this->headers_->remove($key);
    }

    public function getHeader($key)
    {
        return $this->headers_->get($key);
    }

    public function setCookie(
        $key,
        $value = null,
        $expires = null,
        $path = null,
        $domain = null,
        $secure = false,
        $httponly = false)
    {
        $this->cookies_->set(
            $key,
            $value,
            $expires,
            $path,
            $domain,
            $secure,
            $httponly
        );

        return $this;
    }

    public function removeCookie($key)
    {
        $this->cookies_->remove($key);

        return $this;
    }

    public function getCookies()
    {
        return $this->cookies_;
    }

    public function getStatus()
    {
        return $this->status_;
    }

    public function merge(HttpResponse $response)
    {
        $headers = $response->getHeaders();

        foreach ($headers as $key => $val) {
            $this->headers_[$key] = $val;
        }

        $this->status_ = $response->getStatus();

        return $this;
    }

    public function appendContent($append)
    {
        $this->content_ .= $append;

        return $this;
    }

    public function sendHeaders()
    {
        if ($this->headers_has_sent_) {
            return $this;
        }

        header($this->renderStatusLine());

        foreach ($this->getHeaders() as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $s_value) {
                    header($key . ':' . $s_value, false);
                }
            } else {
                header($key . ':' . $value, true);
            }
        }

        $cookies = $this->getCookies();

        if (count($cookies)) {
            header('Set-Cookie:' . $cookies);
        }

        $this->headers_has_sent = true;

        return $this;
    }

    public function sendContent()
    {
        if ($this->content_has_sent_) {
            return $this;
        }

        echo $this->getContent();

        return $this;
    }

    public function send()
    {
        $this->sendHeaders()->sendContent();

        return $this;
    }

    public function __tostring()
    {
        $str = $this->renderStatusLine() . "\r\n"
                . (string) $this->getHeaders()
                . 'Set-Cookie:' . (string) $this->getCookies() . "\r\n"
                . $this->getContent();

        return $str;
    }
}
