<?php
namespace Turbine\Web\Response;

class JSONResponse extends HttpResponse
{
    private $default_content_type_ = 'application/json; charset=utf-8';

    public function __construct($obj, $status = 200, $headers = null)
    {
        $headers = array_merge(
            array('Content-Type' => $this->default_content_type_),
            $headers ?: array()
        );

        parent::__construct(json_encode($obj), $status, $headers);
    }
}
