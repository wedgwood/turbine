<?php
namespace Turbine\Web\Response;

use Turbine\Core\DictBag;

class Cookies extends DictBag
{
    public function __construct(array $cookie = array())
    {
        parent::__construct($cookie);
    }

    public function set(
        $name,
        $value = null,
        $expires = null,
        $path = null,
        $domain = null,
        $secure = false,
        $httponly = false)
    {
        if (!isset($this[$name])) {
            $this[$name] = array();
        }

        if (isset($value)) {
            $this[$name]['value'] = $value;
        }

        if (isset($expires)) {
            $this[$name]['expires'] = $expires;
        }

        if (isset($domain)) {
            $this[$name]['domain'] = $domain;
        }

        if (isset($path)) {
            $this[$name]['path'] = $path;
        }

        if (isset($secure)) {
            $this[$name]['secure'] = $secure;
        }

        if (isset($httponly)) {
            $this[$name]['httponly'] = $httponly;
        }

        return $this;
    }

    public function __tostring()
    {
        $cookies = array();

        foreach ($this as $name => $entry) {
            $line = $name . '=' . rawurlencode($entry['value']);

            if (isset($this['expires'])) {
                $line .= '; Expires=' . $this['expires'];
            }

            if (isset($this['domain'])) {
                $line .= ';Domain=' . $this['domain'];
            }

            if (isset($this['path'])) {
                $line .= ';Path=' . $this['path'];
            }

            if (isset($this['secure']) && $this['secure']) {
                $line .= ';Secure';
            }

            if (isset($this['httponly']) && $this['httponly']) {
                $line .= ';HttpOnly';
            }

            $cookies[] = $line;
        }

        return implode(',', $cookies);
    }
}
