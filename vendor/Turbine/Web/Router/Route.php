<?php
namespace Turbine\Web\Router;

use Turbine\Core;

class Route
{
    public $rule;
    public $method;
    public $callable;
    public $name;
    public $config;

    public function __construct(
        $rule,
        $method,
        $callable,
        $name = null,
        $config = null)
    {
        $this->rule = $rule;
        $this->method = $method;

        if (is_callable($callable)) {
            $this->callable = $callable;
        } else {
            throw new \InvalidArgumentException();
        }

        $this->name = $name ? $name : null;
        $this->config = new Core\DictBag($config);
    }

    public function call($args)
    {
        return call_user_func($this->callable, $args);
    }

    public function __invoke()
    {
        return call_user_func_array($this->callable, func_get_args());
    }
}
