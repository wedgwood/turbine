<?php
namespace Turbine\Web\Router\Filter;

class IntFilter
{
    public static function create($conf)
    {
        return array('-?\d+', 'intval', null);
    }
}
