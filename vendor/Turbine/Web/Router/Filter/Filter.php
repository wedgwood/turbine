<?php
namespace Turbine\Web\Router\Filter;

class Filter
{
    public static function create($mode, $conf)
    {
        $name = '\Turbine\Web\Router\Filter\\' . ucfirst($mode) . 'Filter';
        return $name::create($conf);
    }
}

