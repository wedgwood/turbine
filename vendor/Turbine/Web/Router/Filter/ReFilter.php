<?php
namespace Turbine\Web\Router\Filter;

class ReFilter
{
    private static $default_pattern_ = '[^/]+';

    public static function create($conf)
    {
        return array($conf ? $conf : self::$default_pattern_, null, null);
    }
}
