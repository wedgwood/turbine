<?php
namespace Turbine\Web\Router\Filter;

class FloatFilter
{
    private $conf_;

    public static function create($conf)
    {
        return array('-?[\d\.]+', 'floatval', null);
    }
}
