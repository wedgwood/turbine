<?php
namespace Turbine\Web\Router;

use Turbine\Core\Annotation;
use Turbine\Web\Constant\HttpMethod;

class Routable
{
    public static function __callstatic($method, $arguments)
    {
        $instance = new static();
        $method = substr($method, 4);
        return call_user_func_array(array($instance, $method), $arguments);
    }

    public static function getRoutes()
    {
        $reflection_class = new \ReflectionClass(get_called_class());
        $methods = $reflection_class->getMethods(\ReflectionMethod::IS_PUBLIC);

        $ret = array();

        foreach ($methods as $method) {
            $annotation = new Annotation($method->getDocComment());

            if ($method->isStatic()
                || $method->isConstructor()
                || $method->isDestructor()
            ) {
                continue;
            }

            $name = $method->name;

            if (isset($name[0])
                && isset($name[1])
                && $name[0] == '_'
                && $name[1] == '_'
            ) {
                continue;
            }

            $ret[$name] = array('method' => $name, 'routes' => array());

            $verbs = HttpMethod::getConstants();

            foreach ($verbs as $verb) {
                $verb = strtolower($verb);

                if ($annotation->has($verb)) {
                    $ret[$name]['routes'][] = array(
                        'verb' => $verb,
                        'route' => $annotation->get($verb)
                    );
                }
            }
        }

        return $ret;
    }
}
