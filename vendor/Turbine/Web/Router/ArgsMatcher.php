<?php
namespace Turbine\Web\Router;

class ArgsMatcher
{
    private $re_;
    private $filters_;

    public function __construct($re, $filters)
    {
        $this->re_ = $re;
        $this->filters_ = $filters;
    }

    public function match($path)
    {
        $ret = preg_match($this->re_, $path, $matches);

        array_shift($matches);

        foreach ($this->filters_ as $key => $filter) {
            $matches[$key] = $filter($matches[$key]);
        }

        return $matches;
    }

    public function __invoke($path)
    {
        return $this->match($path);
    }
}

