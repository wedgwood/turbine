<?php
namespace Turbine\Web\CompilerModule;

use Turbine\Compiler\InvokerBag;
use Turbine\Compiler\CompileStage;
use Turbine\CompilerModule\AbstractCompilerModule;
use Turbine\Web\Constant\HttpStatus;
use Turbine\Web\Response\ErrorResponseRenderer;

class ErrorRendererCompilerModule extends AbstractCompilerModule
{
    const COMPILE_STAGE = CompileStage::COMPILE;

    public function compile(InvokerBag $invoker_bag)
    {
        $turbine = \Turbine::getInstance();

        foreach ($invoker_bag as $entry) {
            $annotation = $entry->annotation;
            $invoker = $entry->invoker;

            if ($annotation->has('error')) {
                foreach ($annotation->get('error') as $status) {
                    ErrorResponseRenderer::registerRenderer($status, $invoker);
                }
            }
        }
    }
}
