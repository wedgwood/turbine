<?php
namespace Turbine\Web\CompilerModule;

use Turbine\Compiler\InvokerBag;
use Turbine\Compiler\CompileStage;
use Turbine\CompilerModule\AbstractCompilerModule;
use Turbine\Web\Constant\HttpMethod;

class RouteCompilerModule extends AbstractCompilerModule
{
    const COMPILE_STAGE = CompileStage::COMPILE;

    private static function clearDoubleSlash_($path)
    {
        return implode('/', preg_split('#\/+#', $path));
    }

    public function compile(InvokerBag $invoker_bag)
    {
        $verbs = HttpMethod::getConstants();
        $turbine = \Turbine::getInstance();

        foreach ($invoker_bag as $entry) {
            $annon = $entry->annotation;
            $invoker = $entry->invoker;
            $prefix = $annon->getOne('prefix', '/');
            $route_already = false;

            foreach ($verbs as $verb) {
                $lowerVerb = strtolower($verb);

                if ($annon->has($lowerVerb)) {
                    $route_already = true;

                    foreach ($annon->get($lowerVerb) as $path) {
                        $turbine->route(
                            self::clearDoubleSlash_($prefix . $path),
                            $verb,
                            $invoker
                        );
                    }
                }
            }

            $magic = $annon->getOne('magic', 'off');

            if ($magic == 'any' || ($magic == 'on' && !$route_already)) {
                $name = $invoker->getName();

                $turbine->get(
                    self::clearDoubleSlash_($prefix . $name),
                    $invoker
                );
            }
        }
    }
}
