<?php
namespace Turbine\Web\CompilerModule;

use Turbine\Web\App;
use Turbine\Compiler\InvokerBag;
use Turbine\Compiler\CompileStage;
use Turbine\CompilerModule\AbstractCompilerModule;

class AppCompilerModule extends AbstractCompilerModule
{
    const COMPILE_STAGE = CompileStage::COLLECT;

}
