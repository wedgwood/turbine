<?php
namespace Turbine\Web\Constant;

use Turbine\Core\ConstantBag;

class HttpScheme extends ConstantBag
{
    const HTTP = 'HTTP';
    const HTTPS = 'HTTPS';
}
