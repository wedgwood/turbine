<?php
namespace Turbine\Web\Constant;

use Turbine\Core\ConstantBag;

class HttpVersion extends ConstantBag
{
    const VERSION_10 = '1.0';
    const VERSION_11 = '1.1';
}
