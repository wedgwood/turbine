<?php
namespace Turbine\Web\Constant;

use Turbine\Core\ConstantBag;

class HttpMethod extends ConstantBag
{
    const GET       = 'GET';
    const POST      = 'POST';
    const HEAD      = 'HEAD';
    const DELETE    = 'DELETE';
    const PUT       = 'PUT';
    const ANY       = 'ANY';
}
