<?php
namespace Turbine\Web\App;

class App
{
    const DEFAULT_CONTROLLER_PATH = 'controller/';
    const DEFAULT_MODEL_PATH = 'model/';
    const DEFAULT_VIEW_PATH = 'view/';
    const DEFAULT_CONFIG_PATH = 'config/';

    private static $controller_ = array();
    private static $model_ = array();

    public static function load($path, $config = array())
    {
        $controller_path = isset($config['controller_path']) ?
                    $config['controller_path'] : self::DEFAULT_CONTROLLER_PATH;

        $model_path = isset($config['model_path']) ?
                    $config['model_path'] : self::DEFAULT_MODEL_PATH;


        $view_path = isset($config['view_path']) ?
                    $config['view_path'] : self::DEFAULT_VIEW_PATH;

        $config_path = isset($config['config_path']) ?
                    $config['config_path'] : self::DEFAULT_CONFIG_PATH;

        self::loadController($path . $controller_path);
        self::loadModel($path . $model_path);
        $view_path = $path . $view_path;
        self::loadConfig($path . $config_path);
    }

    public static function loadController($controller_path)
    {
        $class_files = glob($controller_path . '*.php');

        foreach ($class_files as $file) {
            $class = basename($file, '.php');
            self::$controller_[$class] = $file;
        }
    }

    public static function loadModel($model_path)
    {
        $class_files = glob($model_path . '*.php');

        foreach ($class_files as $file) {
            $class = basename($file, '.php');
            self::$model_[$class] = $file;
        }
    }

    public static function loadConfig($config_path)
    {
    }
}
