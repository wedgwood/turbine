<?php
namespace Turbine\Web\Event;

class Event extends \Turbine\Core\Event
{
    const ON_REQUEST   = 'onrequest';
    const BEFORE_ROUTE = 'beforeroute';
    const AFTER_ROUTE  = 'afterroute';
    const BEFORE_HANDLE_REQUEST = 'beforehandlerequest';
    const AFTER_HANDLE_REQUEST  = 'afterhandlerequest';
}
