<?php
namespace Turbine\Web\Event;

class EventData
{
    public $request;
    public $response;
    public $exception;
    public $user_data;
}
