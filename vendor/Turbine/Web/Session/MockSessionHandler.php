<?php
namespace Turbine\Web\Session;

class MockSessionHandler implements \SessionHandlerInterface
{
    private $name_;
    private $session_data_;

    public function __construct($mock_session_data)
    {
        $_SESSION = $mock_session_data;
    }

    public function open($save_path, $name)
    {
        $this->name_ = $name;
        return true;
    }

    public function close()
    {
        return true;
    }

    public function read($session_id)
    {
        return $this->session_data_;
    }

    public function write($session_id, $session_data)
    {
        $this->session_data_ = $session_data_;
        return true;
    }

    public function gc()
    {
        return true;
    }

    public function destroy($session_id)
    {
        return true;
    }
}
