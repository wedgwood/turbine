<?php
namespace Turbine\Cache;

abstract class AbstractCacheEngine
{
    abstract public function setOption($options);

    abstract public function getOption($key);

    abstract public function add($key, $value, $ttl);

    abstract public function cas($key, $token, $new);

    abstract public function clear($delay);

    abstract public function dec($key, $step);

    abstract public function store($key, $value, $ttl);

    abstract public function delete($key, $time = 0);

    abstract public function exists($keys);

    abstract public function get($key, $callback = null);

    abstract public function inc($key, $step = 1);

    public function __call($method, $args)
    {
        return call_user_func_array(array($this, $method), $args);
    }
}

