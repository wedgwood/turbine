<?php
namespace Turbine\Cache;

class MemcachedCache extends AbstractCacheEngine
{
    private $mc_;

    public function __construct()
    {
        $this->mc_ = new Memcached();
    }

    public function setOption($options)
    {
        foreach ($options as $key => $value) {
            if (!$this->mc_->setOption($key, $value)) {
                return false;
            }
        }

        return true;
    }

    public function getOption($key)
    {
        return $this->mc_->getOption($key);
    }

    public function add($key, $value, $ttl = 0)
    {
        return $this->mc_->add($key, $value, $ttl);
    }

    public function cas($key, $token, $new)
    {
        return $this->mc_->add($token, $key, $value);
    }

    public function clear($delay)
    {
        return $this->flush($delay);
    }

    public function dec($key, $step = 1)
    {
        return $this->mc_->decrement($key, $step);
    }

    public function store($key, $value, $ttl = 0)
    {
        return $this->mc_->set($key, $value, $ttl);
    }

    public function delete($key, $time = 0)
    {
        return $this->mc_->delete($key, $time);
    }

    public function exists($keys)
    {
    }

    public function get($key, $callback = null, &$token = null)
    {
        return $this->mc_->get($key, $callback, $token);
    }

    public function inc($key, $step = 1)
    {
        return $this->mc_->increment($key, $step);
    }
}

