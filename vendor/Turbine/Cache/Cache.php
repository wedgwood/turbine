<?php
namespace Turbine\Cache;

class Cache
{
    private static $engines_ = array();

    public static function config($engine, $config = null)
    {
        self::engine($engine)->config($config);
    }

    public static function engine($engine)
    {
        if (!isset(self::$engines_[$engine])) {
            $engine_class = '\\Turbine\\Cache\\'
                . ucfirst(strtolower($engine)) . 'Cache';
            self::$engines_[$engine] = new $engine_class;
        }

        return self::$engines_[$engine];
    }
}
