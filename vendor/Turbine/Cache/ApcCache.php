<?php
namespace Turbine\Cache;

class ApcCache extends AbstractCacheEngine
{
    public function setOption($options)
    {
    }

    public function getOption($key)
    {
    }

    public function add($key, $value, $ttl = 0)
    {
        return apc_add($key, $value, $ttl);
    }

    public function cas($key, $old, $new)
    {
        return apc_cas($key, $old, $new);
    }

    public function clear($delay = 0, $type = 'user')
    {
        return apc_clear_cache($type);
    }

    public function dec($key, $step = 1)
    {
        return apc_dec($key, $step);
    }

    public function store($key, $value, $ttl = 0)
    {
        return apc_store($key, $value, $ttl);
    }

    public function delete($key, $time = 0)
    {
        return apc_delete($key);
    }

    public function exists($keys)
    {
        return apc_exists($keys);
    }

    public function get($key, $callback = null)
    {
        return apc_fetch($key, $success);
    }

    public function inc($key, $step = 1)
    {
        return apc_inc($key, $step);
    }
}

