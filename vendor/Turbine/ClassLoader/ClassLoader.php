<?php
class ClassLoader
{
    private $file_extension_ = '.php';
    private $namespace_;
    private $namespace_separator_ = '\\';
    private $namespace_prefx_regex_;
    private $include_path_;
    public $flag = 0;

    private $load_map_ = array();

    public function __construct($ns = null, $include_path = null)
    {
        $this->namespace_ = $ns;

        if ($ns) {
            $this->namespace_prefix_regex_ =
                '#' . preg_quote($this->namespace_, '#') . '#';
        }

        $this->include_path_ = $include_path;
    }

    public function setNamespaceSeparator($sep)
    {
        $this->namespace_separator_ = $sep;
        return $this;
    }

    public function setIncludePath($include_path)
    {
        $this->include_path_ = $include_path;
        return $this;
    }

    public function getIncludePath()
    {
        return $this->include_path_;
    }

    public function setFileExtension($ext)
    {
        $this->file_extension_ = $ext;
        return $this;
    }

    public function getFileExtension()
    {
        return $this->file_extension_;
    }

    public function register()
    {
        spl_autoload_register(array($this, 'loadClass'));
        return $this;
    }

    public function unregister()
    {
        spl_autoload_unregister(array($this, 'loadClass'));
        return $this;
    }

    public function loadClass($class_name)
    {
        if ($path = $this->findFile($class_name)) {
            require $path;
        }
    }

    public function findFile($class_name)
    {
        if (!isset($this->namespace_prefix_regex_)
            || preg_match($this->namespace_prefix_regex_, $class_name)
        ) {
            $file = '';
            $namespace = '';
            $last_sep = strripos($class_name, $this->namespace_separator_);

            if (false !== $last_sep) {
                $namespace = substr($class_name, 0, $last_sep);
                $class_name = substr($class_name, $last_sep + 1);
                $file = str_replace(
                    $this->namespace_separator_,
                    DIRECTORY_SEPARATOR,
                    $namespace
                ) . DIRECTORY_SEPARATOR;
            }

            $file .= str_replace(
                '_', DIRECTORY_SEPARATOR, $class_name
            ) . $this->file_extension_;

            $path = ($this->include_path_ ?
                $this->include_path_ . DIRECTORY_SEPARATOR : ''
            ) . $file;

            return $path;
        }

        return false;
    }

    public static function getClassFileList($dir, &$list)
    {
        $dirs = array($dir);
        $files = array();

        while ($cur_dir = array_pop($dirs)) {
            $dh = opendir($cur_dir);

            if (!$dh) {
                return false;
            }

            while ($file = readdir($dh)) {
                if ($file[0] == '.') {
                    continue;
                }

                $fullpath = $dir . '/' . $file;

                if (is_dir($fullpath)) {
                    $dirs[] = $fullpath;
                    continue;
                }

                if (substr($fullpath, -4) == '.php') {
                    $files[] = $fullpath;
                }
            }

            closedir($dh);
        }

        $list = $files;
    }

    public function __wakeup()
    {
    }
}
