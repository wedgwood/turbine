<?php
class ApcClassLoader extends ClassLoader
{
    private $map_;
    private $key_;

    public function __construct($key, $ns = null, $include_path = null)
    {
        $this->key_ = $key;
        $this->map_ = apc_fetch($key);

        parent::__construct($ns, $include_path);
    }

    public function loadClass($class_name)
    {
        if (isset($this->map_) && isset($this->map_[$class_name])) {
            require $this->map_[$class_name];
        } elseif ($path = $this->findFile($class_name)) {
            $map = apc_fetch($this->key_);

            if (!$map) {
                $map = array();
            }

            $map[$class_name] = $path;
            apc_store($this->key_, $map);

            require $path;
        }
    }
}
