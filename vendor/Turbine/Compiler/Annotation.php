<?php
namespace Turbine\Compiler;

use Turbine\Core\DictBag;

class Annotation extends DictBag
{
    // #\@\s*(?P<key>\w+)\s*\(\s*(?P<value>.+)\s*\)#
    private static $regex_ = <<<'EOF'
#\@\s*(?P<key>\w+)\s*\(\s*(?P<value>.+(?<!\s))\s*\)#
EOF;

    public function __construct($comment)
    {
        $ret = preg_match_all(
            self::$regex_,
            $comment,
            $matches,
            PREG_SET_ORDER
        );

        $data = array();

        if ($ret) {
            foreach ($matches as $match) {
                if (isset($data[$match['key']])) {
                    $data[$match['key']][] = $match['value'];
                } else {
                    $data[$match['key']] = array($match['value']);
                }
            }

            parent::__construct($data);
        }
    }

    public function getOne($key, $default = null)
    {
        $tmp = $this->get($key);

        return $tmp ? $tmp[0] : $default;
    }
}
