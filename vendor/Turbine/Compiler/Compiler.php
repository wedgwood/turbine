<?php
namespace Turbine\Compiler;

use Turbine\Compiler\Annotation;
use Turbine\CompilerModule\AbstractCompilerModule;
use Turbine\InvokerWrapper\FunctionWrapper;
use Turbine\InvokerWrapper\MethodWrapper;

class Compiler
{
    private static $modules_ = array();
    private static $raw_ = array('function' => array(), 'class' => array());

    public static function registerModule(AbstractCompilerModule $module)
    {
        $stage = $module->getCompileStage();

        if (!isset(self::$modules_[$stage])) {
            self::$modules_[$stage] = array();
        }

        self::$modules_[$stage][] = $module;
    }

    public static function inspectNLoad()
    {
        $functions = get_defined_functions();
        $user_defined_functions = $functions['user'];

        self::load(array('function' => $user_defined_functions));
    }

    public static function load($raw, $append = true)
    {
        if ($append) {
            if (isset($raw['function'])) {
                self::$raw_['function'] = array_merge(
                    self::$raw_['function'],
                    $raw['function']
                );
            }

            if (isset($raw['class'])) {
                self::$raw_['class'] = array_merge(
                    self::$raw_['class'],
                    $raw['class']
                );
            }
        } else {
            if (isset($raw['function'])) {
                self::$raw_['function'] = $raw['function'];
            }

            if (isset($raw['class'])) {
                self::$raw_['class'] = $raw['class'];
            }
        }
    }

    public static function compile()
    {
        $invoker_bag = new InvokerBag();

        if (isset(self::$raw_['function'])) {
            foreach (self::$raw_['function'] as $fun) {
                $reflection_fun = new \ReflectionFunction($fun);
                $annotation = new Annotation($reflection_fun->getDocComment());

                if (count($annotation)) {
                    $invoker_bag->add(
                        new InvokerBagEntry(
                            new FunctionWrapper(
                                $fun,
                                $reflection_fun->getFilename()
                            ),
                            $annotation
                        )
                    );
                }
            }
        }

        if (isset(self::$raw_['class'])) {
            foreach (self::$raw_['class'] as $class) {
                $reflection_class = new \ReflectionClass($class);
                $class_annon = new Annotation(
                    $reflection_class->getDocComment()
                );

                $methods = $reflection_class->getMethods();

                foreach ($methods as $method) {
                    $method_annon = new Annotation($method->getDocComment());
                    $method_annon->merge($class_annon);

                    $invoker_bag->add(
                        new InvokerBagEntry(
                            new MethodWrapper(
                                $class,
                                $method->getName(),
                                $reflection_class->getFilename()
                            ),
                            $method_annon
                        )
                    );
                }
            }
        }

        foreach (CompileStage::getConstants() as $stage) {
            if (isset(self::$modules_[$stage])) {
                foreach (self::$modules_[$stage] as $module) {
                    $module->compile($invoker_bag);
                }
            }
        }
    }
}
