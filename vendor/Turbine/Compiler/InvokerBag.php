<?php
namespace Turbine\Compiler;

use Turbine\Core\DictBag;

class InvokerBag extends DictBag
{
    private static $counter_;

    private static function getUniqKey_()
    {
        return 'turbine.invokermap.' . (self::$counter_ ++) . '.' . uniqid();
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function add(InvokerBagEntry $entry)
    {
        $uniqkey = self::getUniqKey_();
        $this[$uniqkey] = $entry;

        return $uniqkey;
    }
}
