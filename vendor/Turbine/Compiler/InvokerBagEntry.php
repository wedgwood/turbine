<?php
namespace Turbine\Compiler;

class InvokerBagEntry
{
    public $invoker;
    public $annotation;

    public function __construct($invoker, $annotation)
    {
        $this->invoker = $invoker;
        $this->annotation = $annotation;
    }
}
