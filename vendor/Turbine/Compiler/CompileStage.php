<?php
namespace Turbine\Compiler;

use Turbine\Core\ConstantBag;

class CompileStage extends ConstantBag
{
    const COLLECT = 1;
    const PREPARE = 2;
    const COMPILE = 3;
}
