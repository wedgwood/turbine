<?php
namespace Turbine\Core;

class DictBagRO extends DictBag
{
    public function __construct($data = null)
    {
        parent::__construct($data);
    }

    public function setData($data)
    {
        throw new LogicException();
    }

    public function set($key, $value)
    {
        throw new LogicException();
    }

    protected function get_($key, $value)
    {
        $this[$key] = $value;
    }

    public function offsetSet($key, $value)
    {
        throw new LogicException();
    }
}
