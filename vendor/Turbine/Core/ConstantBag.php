<?php
namespace Turbine\Core;

class ConstantBag
{
    public static function has($key)
    {
        return isset(self::$key);
    }

    public static function get($key)
    {
        return isset(self::$key) ? self::$key : null;
    }

    public static function getConstants()
    {
        static $constants = array();

        $called_class = get_called_class();

        if (isset($constants[$called_class])) {
            return $constants[$called_class];
        }

        $reflection_class = new \ReflectionClass($called_class);

        return ($constants[$called_class] = $reflection_class->getConstants());
    }
}
