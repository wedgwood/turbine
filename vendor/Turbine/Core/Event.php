<?php
namespace Turbine\Core;

class Event extends ConstantBag
{
    const ON_INIT     = 'oninit';
    const ON_UNINIT   = 'onuninit';
    const ON_ERROR    = 'onerror';

    private $type_;
    private $data_;

    public $is_stopped = false;

    public function __construct($type, $data = null)
    {
        $this->type_ = $type;
        $this->data_ = $data;
    }

    public function getType()
    {
        return $this->type_;
    }

    public function stopPropagation()
    {
        $this->is_stopped = true;
    }

    public function isStopped()
    {
        return $this->is_stopped;
    }

    public function getData()
    {
        return $this->data_;
    }

    public function setData($data)
    {
        $this->data_ = $data;
    }

    static public function __callstatic($name, $arguments)
    {
        $const_event = @constant('static::TYPE_' . strtoupper($name));
        $event = null;

        if ($const_event) {
            if (isset($arguments) && isset($arguments[0])) {
                $event = new static($const_event, $arguments[0]);
            } else {
                $event = new static($const_event);
            }
        }

        return $event;
    }

    static public function when($e, $data = null)
    {
        return new static($e, $data);
    }
}
