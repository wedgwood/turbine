<?php
namespace Turbine\Core;

class Turbine
{
    private static $instance_ = null;
    private static $default_error_types_ = E_ALL;

    private $observer_list_ = array();

    public function getInvokerMap()
    {
        return $this->invoker_map_;
    }

    public static function getInstance()
    {
        if (is_null(self::$instance_)) {
            self::$instance_ = self::create();
        }

        return self::$instance_;
    }

    protected static function setInstance_($instance)
    {
        self::$instance_ = $instance;
    }

    public static function create()
    {
        return new static();
    }

    public function __destruct()
    {
        $this->fire(Event::ON_UNINIT);
    }

    public function bootstrap()
    {
        $this->fire(Event::ON_INIT);
    }

    public function register($e, $handler)
    {
        if (!isset($this->observer_list_[$e])) {
            $this->observer_list_[$e] = array();
        }

        if (!is_callable($handler)) {
            throw InvalidArgumentException('register handler not callable');
        }

        $this->observer_list_[$e][] = $handler;

        return $this;
    }

    public function unregister($e, $which = null)
    {
        if (is_null($which)) {
            unset($this->observer_list_[$e]);
        } else {
            foreach ($this->observer_list_[$e] as $key => $handler) {
                if ($which == $handler) {
                    unset($this->observer_list_[$key]);
                }
            }
        }

        return $this;
    }

    public function fire($type, $data = null)
    {
        $e = new Event($type, $data);

        if (isset($this->observer_list_[$type])) {
            $observer_list = $this->observer_list_[$type];
            $is_continued = false;

            foreach ($observer_list as $observer) {
                $is_continued = $observer($e);

                if (false === $is_continued || $e->isStopped()) {
                    break;
                }
            }
        }

        return $this;
    }

    public function fireOne($type, $data = null)
    {
        $e = new Event($type, $data);

        if (isset($this->observer_list_[$type])) {
            $observer_list = $this->observer_list_[$type];
            $observer_list[0]($e);
        }

        return $this;
    }

    public function error($handler, $types = null)
    {
        $types = $types ? $types : self::$default_error_types_;

        return set_error_handler($handler, $types);
    }
}
