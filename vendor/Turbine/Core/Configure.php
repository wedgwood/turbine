<?php
namespace Turbine\Core;

class Configure
{
    private static $instance_;
    private $dictbag_;

    public static function getInstance()
    {
        if (!isset(self::$instance_)) {
            self::$instance_ = new static();
        }

        return self::$instance_;
    }

    private static function setInstance_($instance)
    {
        self::$instance_ = $instance;
    }

    public static function load($path)
    {
        $config = include($path);
        return self::getInstance()->dictbag_->merge($config);
    }

    public static function set($key, $value)
    {
        self::getInstance()->dictbag_->set($key, $value);
    }

    public static function get($key, $default = null)
    {
        return self::getInstance()->dictbag_->get($key, $default);
    }

    public static function select($key, $default = null)
    {
        return self::getInstance()->dictbag_->select($key, $default);
    }

    public function __construct()
    {
        $this->dictbag_ = new DictBag();
    }

    public function __wakeup()
    {
        self::setInstance_($this);
    }
}
