<?php
namespace Turbine\Core;

class GlobalDictBag
{
    public static function setData($data)
    {
        static::$data_ = $data;
    }

    public static function set($key, $value)
    {
        static::$data_[$key] = $value;
    }

    public static function get($key, $default = null)
    {
        return isset(static::$data_[$key]) ? static::$data_[$key] : $default;
    }

    public static function has($key)
    {
        return isset(static::$data_[$key]);
    }
}
