<?php
namespace Turbine\Core;

class DictBag extends \ArrayObject
{
    public function __construct($data = null)
    {
        if (is_null($data)) {
            $data = array();
        }

        parent::__construct($data);
    }

    public function setData($data)
    {
        parent::__construct($data);
    }

    public function get($key, $default = null)
    {
        return $this->offsetExists($key) ? $this[$key] : $default;
    }

    public function set($key, $value)
    {
        $this[$key] = $value;
        return $this;
    }

    public function remove($key)
    {
        $this->offsetUnset($key);

        return $this;
    }

    public function has($key)
    {
        return $this->offsetExists($key);
    }

    public function merge($data)
    {
        foreach ($data as $key => $value) {
            $this[$key] = $value;
        }

        return $this;
    }
}
?><?php
namespace Turbine\Core;

class Annotation extends DictBag
{
    // #\@\s*(?P<key>\w+)\s*\(\s*(?P<value>.+)\s*\)#
    private static $regex_ = <<<'EOF'
#\@\s*(?P<key>\w+)\s*\(\s*(?P<value>.+(?<!\s))\s*\)#
EOF;

    public function __construct($comment)
    {
        $ret = preg_match_all(
            self::$regex_,
            $comment,
            $matches,
            PREG_SET_ORDER
        );

        $data = array();

        if ($ret) {
            foreach ($matches as $match) {
                if (isset($data[$match['key']])) {
                    $data[$match['key']][] = $match['value'];
                } else {
                    $data[$match['key']] = array($match['value']);
                }
            }

            parent::__construct($data);
        }
    }
}
?><?php
namespace Turbine\Core;

class Turbine
{
    private static $instance_ = null;
    private static $default_error_types_ = E_ALL;

    private $observer_list_ = array();

    public function getInvokerMap()
    {
        return $this->invoker_map_;
    }

    public static function getInstance()
    {
        if (is_null(self::$instance_)) {
            self::$instance_ = self::create();
        }

        return self::$instance_;
    }

    protected static function setInstance_($instance)
    {
        self::$instance_ = $instance;
    }

    public static function create()
    {
        return new static();
    }

    public function __destruct()
    {
        $this->fire(Event::ON_UNINIT);
    }

    public function bootstrap()
    {
        $this->fire(Event::ON_INIT);
    }

    public function register($e, $handler)
    {
        if (!isset($this->observer_list_[$e])) {
            $this->observer_list_[$e] = array();
        }

        if (!is_callable($handler)) {
            throw InvalidArgumentException('register handler not callable');
        }

        $this->observer_list_[$e][] = $handler;

        return $this;
    }

    public function unregister($e, $which = null)
    {
        if (is_null($which)) {
            unset($this->observer_list_[$e]);
        } else {
            foreach ($this->observer_list_[$e] as $key => $handler) {
                if ($which == $handler) {
                    unset($this->observer_list_[$key]);
                }
            }
        }

        return $this;
    }

    public function fire($type, $data = null)
    {
        $e = new Event($type, $data);

        if (isset($this->observer_list_[$type])) {
            $observer_list = $this->observer_list_[$type];
            $is_continued = false;

            foreach ($observer_list as $observer) {
                $is_continued = $observer($e);

                if (false === $is_continued || $e->isStopped()) {
                    break;
                }
            }
        }

        return $this;
    }

    public function fireOne($type, $data = null)
    {
        $e = new Event($type, $data);

        if (isset($this->observer_list_[$type])) {
            $observer_list = $this->observer_list_[$type];
            $observer_list[0]($e);
        }

        return $this;
    }

    public function error($handler, $types = null)
    {
        $types = $types ? $types : self::$default_error_types_;

        return set_error_handler($handler, $types);
    }
}
?><?php
namespace Turbine\Core;

class Environ extends GlobalDictBag
{
    protected static $data_ = array();
}
?><?php
namespace Turbine\Core;

use Turbine\Core\InvokerMapEntry;

class InvokerMap extends DictBag
{
    private static $counter_;

    private static function getUniqKey()
    {
        return 'turbine.invokermap.' . (self::$counter_ ++) . '.' . uniqid();
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function add(InvokerMapEntry $invoker)
    {
        $uniqKey = self::getUniqKey();
        $this[$uniqKey] = $invoker;

        return $uniqKey;
    }
}
?><?php
namespace Turbine\Core;

class ConstantBag
{
    public static function has($key)
    {
        return isset(self::$key);
    }

    public static function get($key)
    {
        return isset(self::$key) ? self::$key : null;
    }

    public static function getConstants()
    {
        static $constants = array();

        $called_class = get_called_class();

        if (isset($constants[$called_class])) {
            return $constants[$called_class];
        }

        $reflection_class = new \ReflectionClass($called_class);

        return ($constants[$called_class] = $reflection_class->getConstants());
    }
}
?><?php
namespace Turbine\Core;

class TurbineException extends \Exception
{
}
?><?php
namespace Turbine\Core;

class Event extends ConstantBag
{
    const ON_INIT     = 'oninit';
    const ON_UNINIT   = 'onuninit';
    const ON_ERROR    = 'onerror';

    private $type_;
    private $data_;

    public $is_stopped = false;

    public function __construct($type, $data = null)
    {
        $this->type_ = $type;
        $this->data_ = $data;
    }

    public function getType()
    {
        return $this->type_;
    }

    public function stopPropagation()
    {
        $this->is_stopped = true;
    }

    public function isStopped()
    {
        return $this->is_stopped;
    }

    public function getData()
    {
        return $this->data_;
    }

    public function setData($data)
    {
        $this->data_ = $data;
    }

    static public function __callstatic($name, $arguments)
    {
        $const_event = @constant('static::TYPE_' . strtoupper($name));
        $event = null;

        if ($const_event) {
            if (isset($arguments) && isset($arguments[0])) {
                $event = new static($const_event, $arguments[0]);
            } else {
                $event = new static($const_event);
            }
        }

        return $event;
    }

    static public function when($e, $data = null)
    {
        return new static($e, $data);
    }
}
?><?php
namespace Turbine\Core;

class InvokerMapEntry
{
    public $invoker;
    public $annotation;

    public function __construct($invoker, $annotation)
    {
        $this->invoker = $invoker;
        $this->annotation = $annotation;
    }

    public function __invoke()
    {
        return call_user_func_array($this->invoker, func_get_args());
    }
}
?><?php
class GSR
{
    private static $data_ = array();

    public function has($key) {
        return isset(self::$data_[$key]);
    }

    public function set($key, $value) {
        self::$data_[$key] = $value;
    }

    public function get($key, $otherwise = null) {
        return isset(self::$data_[$key]) ?
                self::$data_[$key] : $otherwise;
    }

    public function remove($key) {
        unset(self::$data_[$key]);
    }

    public function pop($key, $otherwise = null) {
        $ret = self::get($key, $otherwise);
        self::remove($key);
        return $ret;
    }
}
?><?php
namespace Turbine\Core;

class GlobalDictBag
{
    public static function setData($data)
    {
        static::$data_ = $data;
    }

    public static function set($key, $value)
    {
        static::$data_[$key] = $value;
    }

    public static function get($key, $default = null)
    {
        return isset(static::$data_[$key]) ? static::$data_[$key] : $default;
    }

    public static function has($key)
    {
        return isset(static::$data_[$key]);
    }
}
?><?php
namespace Turbine\Core;

class DictBagRO extends DictBag
{
    public function __construct($data = null)
    {
        parent::__construct($data);
    }

    public function setData($data)
    {
        throw new LogicException();
    }

    public function set($key, $value)
    {
        throw new LogicException();
    }

    protected function get_($key, $value)
    {
        $this[$key] = $value;
    }

    public function offsetSet($key, $value)
    {
        throw new LogicException();
    }
}
?><?php
namespace Turbine\CompilerModule;

use Turbine\Compiler\CompileStage;
use Turbine\Core\InvokerMap;
use Turbine\Compiler\AbstractCompilerModule;

class EventCompilerModule extends AbstractCompilerModule
{
    public function compile(InvokerMap $invoker_map)
    {
        $turbine = \Turbine::getInstance();
        $events = \Event::getConstants();

        foreach ($invoker_map as &$entry) {
            $annotation = $entry->annotation;
            $invoker = $entry->invoker;

            if ($annotation->has('when')) {
                foreach ($annotation->get('when') as $when) {
                    $turbine->register($when, $invoker);
                }
            }
        }
    }

    public function getCompileStage()
    {
        return CompileStage::COMIPLE;
    }
}
?><?php
namespace Turbine\CompilerModule;

use Turbine\Core\InvokerMap;

use Turbine\Cache\Cache;
use Turbine\Cache\CacheInvokerWrapper;

use Turbine\Compiler\CompileStage;
use Turbine\Compiler\AbstractCompilerModule;

class CacheCompilerModule extends AbstractCompilerModule
{
    private static $regex_ = <<<'EOF'
#\s*(?P<ttl>\d+)\s*,\s*(?P<engine>.+(?<!\s))?#
EOF;

    private static $default_cache_engine_ = '';

    public static function setDefaultCacheEngine($engine)
    {
        self::$default_cache_engine_ = $engine;
    }

    public function compile(InvokerMap $invoker_map)
    {
        $turbine = \Turbine::getInstance();
        $events = \Event::getConstants();

        foreach ($invoker_map as &$entry) {
            $annotation = $entry->annotation;
            $invoker = $entry->invoker;

            if ($annotation->has('cache')) {
                $config = $annotation->get('cache');
                $match_cnt = preg_match(self::$regex_, $config[0], $match);

                $ttl = 0;
                $engine = null;

                if ($match_cnt) {
                    $ttl = $match['ttl'];
                    $engine = empty($match['engine']) ?
                        self::$default_cache_engine_ : $match['engine'];
                } else {
                    $engine = self::$default_cache_engine_;
                }

                $entry->invoker = new CacheInvokerWrapper(
                                    $invoker, $engine, $ttl);
            }
        }
    }

    public function getCompileStage()
    {
        return CompileStage::PREPARE;
    }
}
?><?php
namespace Turbine\Compiler;

use Turbine\Core\Annotation;
use Turbine\Core\InvokerMap;
use Turbine\Core\InvokerMapEntry;

class Compiler
{
    private static $modules_ = array();

    public static function registerModule(AbstractCompilerModule $module)
    {
        $stage = $module->getCompileStage();

        if (!isset(self::$modules_[$stage])) {
            self::$modules_[$stage] = array();
        }

        self::$modules_[$stage][] = $module;
    }

    public static function compile()
    {
        $fs = get_defined_functions();
        $fs = $fs['user'];

        $invoker_map = new InvokerMap();

        foreach ($fs as $f) {
            $reflection_fun = new \ReflectionFunction($f);
            $annotation = new Annotation($reflection_fun->getDocComment());

            if (count($annotation)) {
                $invoker_map->add(new InvokerMapEntry($f, $annotation));
            }
        }

        foreach (CompileStage::getConstants() as $stage) {
            if (isset(self::$modules_[$stage])) {
                foreach (self::$modules_[$stage] as $module) {
                    $module->compile($invoker_map);
                }
            }
        }
    }
}
?><?php
namespace Turbine\Compiler;

use Turbine\Core\InvokerMap;

abstract class AbstractCompilerModule
{
    public static function register()
    {
        Compiler::registerModule(new static());
    }

    abstract public function compile(InvokerMap $invoker_map);

    abstract public function getCompileStage();
}
?><?php
namespace Turbine\Compiler;

use Turbine\Core\ConstantBag;

class CompileStage extends ConstantBag
{
    const PREPARE = 1;
    const COMIPLE = 2;
}
?><?php
namespace Turbine\Cache;

class CacheInvokerWrapper
{
    private $cache_engine_type_;
    private $invoker_;
    private $ttl_;
    private $key_;

    static private $counter_ = 0;

    private static function getUniqKey()
    {
        return 'turbine.cache.invoker.' . (self::$counter_ ++) . '.' . uniqid();
    }

    public function __construct($invoker, $engine = null, $ttl = 0)
    {
        $this->cache_engine_type_ = $engine;
        $this->invoker_ = $invoker;
        $this->ttl_ = $ttl;

        $this->key_ = self::getUniqKey();
    }

    public function __invoke()
    {
        $engine = Cache::engine($this->cache_engine_type_);
        $ret = $engine->get($this->key_);

        if (false === $ret) {
            $ret = call_user_func_array($this->invoker_, func_get_args());
            $engine->store($this->key_, $ret, $this->ttl_);
        }

        return $ret;
    }
}
?><?php
namespace Turbine\Cache;

class Cache
{
    private static $engines_ = array();

    public static function config($engine, $config = null)
    {
        self::engine($engine)->config($config);
    }

    public static function engine($engine)
    {
        if (!isset(self::$engines_[$engine])) {
            $engine_class = '\\Turbine\\Cache\\'
                . ucfirst(strtolower($engine)) . 'Cache';
            self::$engines_[$engine] = new $engine_class;
        }

        return self::$engines_[$engine];
    }
}
?><?php
namespace Turbine\Cache;

class ApcCache extends AbstractCacheEngine
{
    public function setOption($options)
    {
    }

    public function getOption($key)
    {
    }

    public function add($key, $value, $ttl = 0)
    {
        return apc_add($key, $value, $ttl);
    }

    public function cas($key, $old, $new)
    {
        return apc_cas($key, $old, $new);
    }

    public function clear($delay = 0, $type = 'user')
    {
        return apc_clear_cache($type);
    }

    public function dec($key, $step = 1)
    {
        return apc_dec($key, $step);
    }

    public function store($key, $value, $ttl = 0)
    {
        return apc_store($key, $value, $ttl);
    }

    public function delete($key, $time = 0)
    {
        return apc_delete($key);
    }

    public function exists($keys)
    {
        return apc_exists($keys);
    }

    public function get($key, $callback = null)
    {
        return apc_fetch($key, $success);
    }

    public function inc($key, $step = 1)
    {
        return apc_inc($key, $step);
    }
}

?><?php
namespace Turbine\Cache;

class MemcachedCache extends AbstractCacheEngine
{
    private $mc_;

    public function __construct()
    {
        $this->mc_ = new Memcached();
    }

    public function setOption($options)
    {
        foreach ($options as $key => $value) {
            if (!$this->mc_->setOption($key, $value)) {
                return false;
            }
        }

        return true;
    }

    public function getOption($key)
    {
        return $this->mc_->getOption($key);
    }

    public function add($key, $value, $ttl = 0)
    {
        return $this->mc_->add($key, $value, $ttl);
    }

    public function cas($key, $token, $new)
    {
        return $this->mc_->add($token, $key, $value);
    }

    public function clear($delay)
    {
        return $this->flush($delay);
    }

    public function dec($key, $step = 1)
    {
        return $this->mc_->decrement($key, $step);
    }

    public function store($key, $value, $ttl = 0)
    {
        return $this->mc_->set($key, $value, $ttl);
    }

    public function delete($key, $time = 0)
    {
        return $this->mc_->delete($key, $time);
    }

    public function exists($keys)
    {
    }

    public function get($key, $callback = null, &$token = null)
    {
        return $this->mc_->get($key, $callback, $token);
    }

    public function inc($key, $step = 1)
    {
        return $this->mc_->increment($key, $step);
    }
}

?><?php
namespace Turbine\Cache;

abstract class AbstractCacheEngine
{
    abstract public function setOption($options);

    abstract public function getOption($key);

    abstract public function add($key, $value, $ttl);

    abstract public function cas($key, $token, $new);

    abstract public function clear($delay);

    abstract public function dec($key, $step);

    abstract public function store($key, $value, $ttl);

    abstract public function delete($key, $time = 0);

    abstract public function exists($keys);

    abstract public function get($key, $callback = null);

    abstract public function inc($key, $step = 1);

    public function __call($method, $args)
    {
        return call_user_func_array(array($this, $method), $args);
    }
}

?><?php
namespace Turbine\Web\Response;

class ErrorResponseRenderer
{
    private static $renderers_ = array();

    public static function registerRenderer($status, $renderer)
    {
        if (isset(self::$renderers_[$status])) {
            self::$renderers_[$status][] = $renderer;
        } else {
            self::$renderers_[$status] = array($renderer);
        }
    }

    public static function render(ErrorResponse $response)
    {
        $status = $response->getStatus();

        if (isset(self::$renderers_[$status])) {
            foreach (self::$renderers_[$status] as $render) {
                $resp = $render($response);

                if ($resp instanceof HttpResponse) {
                    $response = $resp;
                } elseif (!is_null($resp)) {
                    $response = new HttpResponse((string) $resp);
                }
            }
        }

        return $response;
    }
}
?><?php
namespace Turbine\Web\Response;

class JSONResponse extends HttpResponse
{
    private $default_content_type_ = 'application/json; charset=utf-8';

    public function __construct($obj, $status = 200, $headers = null)
    {
        $headers = array_merge(
            array('Content-Type' => $this->default_content_type_),
            $headers ?: array()
        );

        parent::__construct(json_encode($obj), $status, $headers);
    }
}
?><?php
namespace Turbine\Web\Response;

use Turbine\Web\Constant\HttpStatus,
    Turbine\Web\Constant\HttpVersion;

class HttpResponse extends \Exception
{
    private $content_;
    private $status_;
    private $reason_;
    private $headers_;
    private $cookies_;
    private $version_ = HttpVersion::VERSION_10;

    private $headers_has_sent_ = false;
    private $content_has_sent_ = false;

    private $default_content_type_ = 'text/html; charset=utf-8';

    public static function getInstance()
    {
        static $instance;

        if (isset($instance)) {
            return $instance;
        }

        return ($instance = new static());
    }

    public function __construct($content = '', $status = 200, $headers = null)
    {
        if (is_array($headers)) {
            $headers = array_merge(
                array('Content-Type' => $this->default_content_type_),
                $headers
            );
        }

        $this->headers_ = new HttpHeaders($headers);
        $this->cookies_ = new Cookies();
        $this->status_ = $status;
        $this->setContent($content);
    }

    public function getContent()
    {
        return $this->content_;
    }

    public function setContent($content)
    {
        $this->content_ = $content;
        $this->setHeader('Content-Length', strlen($this->content_));

        return $this;
    }

    public function setStatus($status, $reason = null)
    {
        $this->status_ = $status;

        if (!is_null($reason)) {
            $this->setReasonPhrase($reason);
        }

        return $this;
    }

    public function setReasonPhrase($reason)
    {
        $this->reason_ = trim($reason);

        return $this;
    }

    public function getReasonPhrase()
    {
        if (is_null($this->reason_)) {
            return HttpStatus::getReasonPhrase($this->status_);
        }

        return $this->reason_;
    }

    public function setVersion($version)
    {
        if ($version != HttpVersion::VERSION_10
            && $version != HttpVersion::VERSION_11
        ) {
            throw new InvalidArgumentException();
        }

        $this->version_ = $version;

        return $this;
    }

    public function getVersion()
    {
        return $this->version_;
    }

    public function renderStatusLine()
    {
        $status_line = sprintf(
            'HTTP/%s %d %s',
            $this->getVersion(),
            $this->getStatus(),
            $this->getReasonPhrase()
        );

        return trim($status_line);
    }

    public function getHeaders()
    {
        return $this->headers_;
    }

    public function setHeader($key, $value)
    {
        $this->headers_->set($key, $value);

        return $this;
    }

    public function removeHeader($key)
    {
        $this->headers_->remove($key);
    }

    public function getHeader($key)
    {
        return $this->headers_->get($key);
    }

    public function setCookie(
        $key,
        $value = null,
        $expires = null,
        $path = null,
        $domain = null,
        $secure = false,
        $httponly = false)
    {
        $this->cookies_->set(
            $key,
            $value,
            $expires,
            $path,
            $domain,
            $secure,
            $httponly
        );

        return $this;
    }

    public function removeCookie($key)
    {
        $this->cookies_->remove($key);

        return $this;
    }

    public function getCookies()
    {
        return $this->cookies_;
    }

    public function getStatus()
    {
        return $this->status_;
    }

    public function merge(HttpResponse $response)
    {
        $headers = $response->getHeaders();

        foreach ($headers as $key => $val) {
            $this->headers_[$key] = $val;
        }

        $this->status_ = $response->getStatus();

        return $this;
    }

    public function appendContent($append)
    {
        $this->content_ .= $append;

        return $this;
    }

    public function sendHeaders()
    {
        if ($this->headers_has_sent_) {
            return $this;
        }

        header($this->renderStatusLine());

        foreach ($this->getHeaders() as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $s_value) {
                    header($key . ':' . $s_value, false);
                }
            } else {
                header($key . ':' . $value, true);
            }
        }

        $cookies = $this->getCookies();

        if (count($cookies)) {
            header('Set-Cookie:' . $cookies);
        }

        $this->headers_has_sent = true;

        return $this;
    }

    public function sendContent()
    {
        if ($this->content_has_sent_) {
            return $this;
        }

        echo $this->getContent();

        return $this;
    }

    public function send()
    {
        $this->sendHeaders()->sendContent();

        return $this;
    }

    public function __tostring()
    {
        $str = $this->renderStatusLine() . "\r\n"
                . (string) $this->getHeaders()
                . 'Set-Cookie:' . (string) $this->getCookies() . "\r\n"
                . $this->getContent();

        return $str;
    }
}
?><?php
namespace Turbine\Web\Response;

use Turbine\Core\DictBag;

class Cookies extends DictBag
{
    public function __construct(array $cookie = array())
    {
        parent::__construct($cookie);
    }

    public function set(
        $name,
        $value = null,
        $expires = null,
        $path = null,
        $domain = null,
        $secure = false,
        $httponly = false)
    {
        if (!isset($this[$name])) {
            $this[$name] = array();
        }

        if (isset($value)) {
            $this[$name]['value'] = $value;
        }

        if (isset($expires)) {
            $this[$name]['expires'] = $expires;
        }

        if (isset($domain)) {
            $this[$name]['domain'] = $domain;
        }

        if (isset($path)) {
            $this[$name]['path'] = $path;
        }

        if (isset($secure)) {
            $this[$name]['secure'] = $secure;
        }

        if (isset($httponly)) {
            $this[$name]['httponly'] = $httponly;
        }

        return $this;
    }

    public function __tostring()
    {
        $cookies = array();

        foreach ($this as $name => $entry) {
            $line = $name . '=' . rawurlencode($entry['value']);

            if (isset($this['expires'])) {
                $line .= '; Expires=' . $this['expires'];
            }

            if (isset($this['domain'])) {
                $line .= ';Domain=' . $this['domain'];
            }

            if (isset($this['path'])) {
                $line .= ';Path=' . $this['path'];
            }

            if (isset($this['secure']) && $this['secure']) {
                $line .= ';Secure';
            }

            if (isset($this['httponly']) && $this['httponly']) {
                $line .= ';HttpOnly';
            }

            $cookies[] = $line;
        }

        return implode(',', $cookies);
    }
}
?><?php
namespace Turbine\Web\Response;

class ErrorResponse extends HttpResponse
{
    private $exception_;
    private $traceback_;

    public function __construct(
        $status = 500,
        $content = '',
        $exception = null,
        $traceback = null,
        $headers = null)
    {
        parent::__construct($content, $status, $headers);

        $this->traceback_ = $traceback;
        $this->exception_ = $exception;
    }

    public function __tostring()
    {
        // TODO: some control given to user
        return $this->getStatus();
    }
}
?><?php
namespace Turbine\Web\Response;

class HttpHeaders extends \Turbine\Core\DictBag
{
    private $has_sent_ = false;

    public function __construct($headers = null)
    {
        parent::__construct($headers);
    }

    public function __tostring()
    {
        $ret = '';

        foreach ($this as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $s_value) {
                    $ret .= $key . ':' . $s_value . "\r\n";
                }
            } else {
                $ret .= $key . ':' . $value . "\r\n";
            }
        }

        return $ret;
    }
}
?><?php
namespace Turbine\Web\Constant;

class HttpStatus
{
    private static $status_codes_ = array(
        // Informational 1xx
        100 => 'Continue',
        101 => 'Switching Protocols',
        // Successful 2xx
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        // Redirection 3xx
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => '(Unused)',
        307 => 'Temporary Redirect',
        // Client Error 4xx
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot', // RFC 2324
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        // Server Error 5xx
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        511 => 'Network Authentication Required'
    );

    public static function getReasonPhrase($code)
    {
        return isset(self::$status_codes_[$code]) ?
                    self::$status_codes_[$code] : '';
    }
}
?><?php
namespace Turbine\Web\Constant;

use Turbine\Core\ConstantBag;

class HttpScheme extends ConstantBag
{
    const HTTP = 'HTTP';
    const HTTPS = 'HTTPS';
}
?><?php
namespace Turbine\Web\Constant;

use Turbine\Core\ConstantBag;

class HttpVersion extends ConstantBag
{
    const VERSION_10 = '1.0';
    const VERSION_11 = '1.1';
}
?><?php
namespace Turbine\Web\Constant;

use Turbine\Core\ConstantBag;

class HttpMethod extends ConstantBag
{
    const GET       = 'GET';
    const POST      = 'POST';
    const HEAD      = 'HEAD';
    const DELETE    = 'DELETE';
    const PUT       = 'PUT';
    const ANY       = 'ANY';
}
?><?php
namespace Turbine\Web\CompilerModule;

use Turbine\Core\InvokerMap;
use Turbine\Compiler\AbstractCompilerModule;
use Turbine\Compiler\CompileStage;
use Turbine\Web\Constant\HttpMethod;

class RouteCompilerModule extends AbstractCompilerModule
{
    public function compile(InvokerMap $invoker_map)
    {
        $verbs = HttpMethod::getConstants();
        $turbine = \Turbine::getInstance();

        foreach ($invoker_map as &$entry) {
            $annotation = $entry->annotation;
            $invoker = $entry->invoker;

            foreach ($verbs as $verb) {
                $lowerVerb = strtolower($verb);

                if ($annotation->has($lowerVerb)) {
                    foreach ($annotation->get($lowerVerb) as $path) {
                        $turbine->route($path, $verb, $invoker);
                    }
                }
            }
        }
    }

    public function getCompileStage()
    {
        return CompileStage::COMIPLE;
    }
}

?><?php
namespace Turbine\Web\CompilerModule;

use Turbine\Core\InvokerMap;
use Turbine\Compiler\AbstractCompilerModule;
use Turbine\Compiler\CompileStage;
use Turbine\Web\Constant\HttpStatus;
use Turbine\Web\Response\ErrorResponseRenderer;

class ErrorRendererCompilerModule extends AbstractCompilerModule
{
    public function compile(InvokerMap $invoker_map)
    {
        $turbine = \Turbine::getInstance();

        foreach ($invoker_map as &$entry) {
            $annotation = $entry->annotation;
            $invoker = $entry->invoker;

            if ($annotation->has('error')) {
                foreach ($annotation->get('error') as $status) {
                    ErrorResponseRenderer::registerRenderer($status, $invoker);
                }
            }
        }
    }

    public function getCompileStage()
    {
        return CompileStage::COMIPLE;
    }
}
?><?php
namespace Turbine\Web\App;

class App
{

    public static function load($name, $path)
    {
    }

    private $name_;
    private $path_;
    private $controller_path_ = 'controller/';
    private $model_path_ = 'model/';
    private $view_path_ = 'view/';

    public function __construct($config, $name = null)
    {
        if (isset($config['controller'])) {
            $this->controller_path_ = $config['controller'];
        }

        if (isset($config['model'])) {
            $this->model_path_ = $config['model'];
        }

        if (isset($config['view'])) {
            $this->view_path_ = $config['view'];
        }

        if (isset($config['config'])) {
            $this->config_path_ = $config['config'];
        }

        if (isset($name)) {
            $this->name_ = $name;
        }
    }

    public function load_()
    {
    }

    public function __invoke()
    {

    }
}
?><?php
namespace Turbine\Web\App;

use Turbine\Web\Router\Routable;

class AppController extends Routable
{

}

?><?php
namespace Turbine\Web;

use Turbine\Core,
    Turbine\Core\Environ,
    Turbine\Web\Constant\HttpStatus,
    Turbine\Web\Event\Event,
    Turbine\Web\Event\EventData,
    Turbine\Web\Router\Route,
    Turbine\Web\Router\Router,
    Turbine\Web\Request\HttpRequest,
    Turbine\Web\Response\HttpResponse,
    Turbine\Web\Response\JSONResponse,
    Turbine\Web\Response\ErrorResponse,
    Turbine\Web\Response\ErrorResponseRenderer;

class Turbine extends Core\Turbine
{
    private $auto_json_;
    private $catch_all_;
    private $router_;
    private $routes_ = array();

    private $error_handler_ = array();

    public function __construct($catch_all = true, $auto_json = true)
    {
        $this->catch_all_ = $catch_all;
        $this->auto_json_ = $auto_json;

        $this->router_ = new Router();
    }

    public function bootstrap()
    {
        parent::bootstrap();

        $request = new HttpRequest();
        $response = null;
        $data = new EventData();

        try {
            $data->request = $request;
            Environ::set('__request', $request);

            $this->fire(Event::ON_REQUEST, $data);
            $this->fire(Event::BEFORE_ROUTE, $data);

            if (is_null($data->response)) {
                $pair = $this->router_->match($request);

                $this->fire(Event::AFTER_ROUTE, $data);
                $this->fire(Event::BEFORE_HANDLE_REQUEST, $data);

                if (isset($pair) || isset($pair[0])) {
                    $params = null;

                    if ($pair[1]) {
                        $params = $pair[1];
                    }

                    $params[] = $request;
                    $response = call_user_func_array($pair[0], $params);
                } else {
                    notFound();
                }
            }

            if (is_array($response) && $this->auto_json_) {
                $response = new JSONResponse($response);
            } elseif (!($response instanceof HttpResponse)) {
                $response = new HttpResponse($response);
            }
        } catch (ErrorResponse $e) {
            $response = ErrorResponseRenderer::render($e);
        } catch (HttpResponse $e) {
            $response = $e;
        } catch (Exception $e) {
            if (!$this->catch_all_) {
                throw $e;
            }

            $data->exception = $e;

            try {
                $this->fire(Event::ON_ERROR, $data);
            } catch (HttpResponse $e) {
                $response = $e;
            } catch (Exception $e) {
                throw $e;
            }

            if (!isset($response)) {
                $response = $data->response;
            }
        }

        $data->response = $response;

        $this->fire(Event::AFTER_HANDLE_REQUEST, $data);
        $this->output($response);
    }

    public function output($response)
    {
        $headers = Environ::get('__headers', null);

        if (isset($headers)) {
            foreach ($headers as $key => $value) {
                $response->setHeader($key, $value);
            }
        }

        $cookies = Environ::get('__cookies', null);

        if (isset($cookies)) {
            foreach ($cookies as $key => $cookie) {
                $response->setCookie(
                    $key,
                    $cookie['value'],
                    $cookie['expires'],
                    $cookie['path'],
                    $cookie['domain'],
                    $cookie['secure'],
                    $cookie['httponly']
                );
            }
        }

        $response->send();
    }

    public function addRoute(Route $route)
    {
        $this->routes_[] = $route;
        $this->router_->add($route);

        return $this;
    }

    public function addRoutable($path, $routable)
    {
        $routes = $routable::getRoutes();

        foreach ($routes as $entry) {
            if (empty($entry['routes'])) {
                $this->get(
                    $path . $entry['method'] . '/',
                    array($routable, '__r_' . $entry['method'])
                );
            } else {
                foreach ($entry['routes'] as $route) {
                    $this->route(
                        $route['route'],
                        $route['verb'],
                        array(
                            $routable,
                            '__r_' . $entry['method']
                        )
                    );
                }
            }
        }
    }

    public function route(
        $path,
        $method,
        $callable,
        $name = null,
        $config = null)
    {
        if (!is_array($path)) {
            $path = array($path);
        }

        if (!is_array($method)) {
            $method = array($method);
        }

        foreach ($path as $rule) {
            foreach ($method as $verb) {
                $verb = strtoupper($verb);
                $route = new Route($rule, $verb, $callable, $name, $config);
                $this->addRoute($route);
            }
        }

        return $this;
    }

    public function get(
        $path,
        $callable,
        $name = null,
        $config = null)
    {
        return $this->route($path, 'GET', $callable, $name, $config);
    }

    public function post(
        $path,
        $callable,
        $name = null,
        $config = null)
    {
        return $this->route($path, 'POST', $callable, $name, $config);
    }

    public function put(
        $path,
        $callable,
        $name = null,
        $config = null)
    {
        return $this->route($path, 'PUT', $callable, $name, $config);
    }

    public function delete(
        $path,
        $callable,
        $name = null,
        $config = null)
    {
        return $this->route($path, 'DELETE', $callable, $name, $config);
    }

    public function __wakeup()
    {
        self::setInstance_($this);
    }
}
?><?php
namespace Turbine\Web\Session;

class MockSessionHandler implements \SessionHandlerInterface
{
    private $name_;
    private $session_data_;

    public function __construct($mock_session_data)
    {
        $_SESSION = $mock_session_data;
    }

    public function open($save_path, $name)
    {
        $this->name_ = $name;
        return true;
    }

    public function close()
    {
        return true;
    }

    public function read($session_id)
    {
        return $this->session_data_;
    }

    public function write($session_id, $session_data)
    {
        $this->session_data_ = $session_data_;
        return true;
    }

    public function gc()
    {
        return true;
    }

    public function destroy($session_id)
    {
        return true;
    }
}
?><?php
function template($tpl_name, $values = null)
{
    $te = \TemplateEngine::getInstance();

    if (isset($values)) {
        $te->assign($values);
    }

    return $te->fetch($tpl_name);
}

function templateCached($tpl_name)
{
    $te = \TemplateEngine::getInstance();

    return $te->isCached($tpl_name);
}
?><?php
use \Turbine\Web\Request\HttpResquest,
    \Turbine\Web\Response\HttpResponse,
    \Turbine\Web\Response\ErrorResponse,
    \Turbine\Web\Constant\HttpVersion;

function redirect($url, $status = null)
{
    if (!$status) {
        $request = HttpRequest::getInstance();
        $status = 'HTTP/' . HttpVersion::VERSION_11
                    == $request->getServer('SERVER_PROTOCOL') ? 303 : 302;
    }

    throw new HttpResponse('', $status, array('Location' => $url));
}

function notFound($text = '', $status = 404)
{
    throw new ErrorResponse($status, $text);
}

function notModified()
{
    throw new HttpResponse('', 304);
}

function abort($text = 'Unknown Error: Application Stopped.', $status = 500)
{
    throw new ErrorResponse($status, $text);
}

function internalServerError($text = '', $status = 500)
{
    throw new ErrorResponse($status, $text);
}
?><?php
namespace Turbine\Web\Op;

use Turbine\Core\Environ;

class Header
{
    public static function get($name = null, $default = null)
    {
        $headers = Environ::get('__headers', null);

        if (is_null($name)) {
            return $headers;
        }

        if (isset($headers) && isset($headers[$name])) {
            return $headers;
        }

        return $default;
    }

    public static function set($name, $value)
    {
        $headers = Environ::get('__headers', array());
        $headers[$name] = $value;

        Environ::set('__headers', $headers);
    }

    public static function has($name)
    {
        $headers = Environ::get('__headers', null);

        return !empty($headers) && isset($headers[$name]);
    }
}
?><?php
namespace Turbine\Web\Op;

use Turbine\Core\Environ,
    Turbine\Web\Request\HttpRequest;

class Cookie
{
    public static function get($name = null, $default = null)
    {
        $cookies = Environ::get('__cookies', null);

        if (is_null($name)) {
            return $cookies;
        }

        if (isset($cookies) && isset($cookies[$name])) {
            return $cookies[$name];
        }

        return $default;
    }

    public static function set(
        $name,
        $value = null,
        $expires = null,
        $path = null,
        $domain = null,
        $secure = false,
        $httponly = false)
    {
        $cookies = Environ::get('__cookies', array());
        $cookies[$name] = array(
            'value'     => $value,
            'expires'   => $expires,
            'path'      => $path,
            'domain'    => $domain,
            'secure'    => $secure,
            'httponly'  => $httponly
        );

        Environ::set('__cookies', $cookies);
    }

    public static function has($name)
    {
        $cookies = Environ::get('__cookies', null);

        return !empty($cookies) && isset($cookies[$name]);
    }

    // TODO: expire the cookie
    public static function expire($name)
    {

    }
}
?><?php
use Turbine\Web\Turbine;

function route($rule, $method, $callable)
{
    Turbine::getInstance()->route($rule, $method, $callable);
}

function get($rule, $callable)
{
    Turbine::getInstance()->get($rule, $callable);
}

function put($rule, $callable)
{
    Turbine::getInstance()->put($rule, $callable);
}

function post($rule, $callable)
{
    Turbine::getInstance()->post($rule, $callable);
}

function delete($rule, $callable)
{
    Turbine::getInstance()->delete($rule, $callable);
}

function error($callable)
{
    Turbine::getInstance()->error($callable);
}
?><?php
use Turbine\Web\Turbine;

function onRequest($callable)
{
    return Turbine::getInstance()->register(Event::ON_REQUEST, $callable);
}

function beforeRoute($callable)
{
    return Turbine::getInstance()->register(Event::BEFORE_ROUTE, $callable);
}

function afterRoute($callable)
{
    return Turbine::getInstance()->register(Event::AFTER_ROUTE, $callable);
}

function beforeHandleRequest($callable)
{
    return Turbine::getInstance()->register(
        Event::BEFORE_HANDLE_REQUEST,
        $callable
    );
}

function afterHandleRequest($callable)
{
    return Turbine::getInstance()->register(
        Event::AFTER_HANDLE_REQUEST,
        $callable
    );
}

function when($when, $callable)
{
    return Turbine::getInstance()->register($when, $callable);
}
?><?php
namespace Turbine\Web\Op;

class Session
{
    private static $is_started_ = false;
    private static $is_destroyed_ = false;
    private static $is_write_closed_ = false;

    private static $session_handler_ = null;

    private static $session_ = null;
    private static $session_id_ = null;

    public static function start($is_started = false)
    {
        if (self::$is_started_ && self::$is_destroyed_) {
            throw new LogicException(
                'session has already started and destroyed'
            );
        }

        if (!self::$is_started_) {
            session_start();

            self::$is_started_ = true;
        }
    }

    public static function isStarted()
    {
        return self::$is_started_;
    }

    public static function distory()
    {
        if (self::$is_destroyed_) {
            return;
        }

        session_destroy();

        self::$is_destroyed_ = true;
    }

    public static function isDestroyed()
    {
        return self::$is_destroyed_;
    }

    public static function setID($id)
    {
        if (!is_string($id) || empty($id)) {
            throw new InvalidArgumentException(
                'invalid parameter $id, session id must be typeof string'
            );
        }

        session_id($id);
    }

    public static function getID()
    {
        return session_id();
    }

    public static function setSessionHandler(SessionHandlerInterface $handler)
    {
        session_set_save_handler(
            array($handler, 'open'),
            array($handler, 'close'),
            array($handler, 'read'),
            array($handler, 'write'),
            array($handler, 'destroy'),
            array($handler, 'gc')
        );

        self::$session_handler_ = $handler;
    }

    public static function getSessionHandler()
    {
        return self::$session_handler_;
    }

    public static function writeClose()
    {
        if (self::$is_write_closed_) {
            return;
        }

        session_write_close();

        self::$is_write_closed_ = false;
    }

    public static function isWritable()
    {
        return self::$is_write_closed_;
    }

    public static function exists()
    {
        $name = session_name();

        if (1 == ini_get('session.use_cookies') && isset($_COOKIE[$name])) {
            return true;
        } elseif (!empty($_REQUEST[$name])) {
            return true;
        }

        return false;
    }

    public static function setCookieLifeTime($seconds = 0)
    {
        $cookie_params = session_get_cookie_params();

        session_set_cookie_params(
            $seconds,
            $cookie_params['path'],
            $cookie_params['domain'],
            $cookie_params['secure'],
            $cookie_params['httponly']
        );
    }

    public static function expireSessionCookie()
    {
        self::setCookieLifeTime(0);
    }

    public static function has($key)
    {
        return isset($_SESSION[$key]);
    }

    public static function get($key, $default = null)
    {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : $default;
    }

    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public static function getIterator()
    {
        return $_SESSION;
    }

    public static function mock($session)
    {
        $_SESSION = $session;
    }
}
?><?php
use Turbine\Core\Annotation;
use Turbine\Web\Constant\HttpMethod;

function compileAutoRoutes()
{
    $funs = get_defined_functions();
    $funs = $funs['user'];
    $verbs = HttpMethod::getConstants();

    foreach ($funs as $fun) {
        $reflection_fun = new ReflectionFunction($fun);
        $annotation = new Annotation($reflection_fun->getDocComment());

        foreach ($verbs as $verb) {
            $lowerVerb = strtolower($verb);

            if ($annotation->has($lowerVerb)) {
                route($annotation->get($lowerVerb), $verb, $fun);
            }
        }
    }
}
?><?php
namespace Turbine\Web\Op;

class TemplateEngine
{
    private static $engine_;
    private static $config_ = null;

    public static function setEngine($engine, $config = null)
    {
        self::$engine_ = $engine;

        if (isset($config)) {
            self::$config_ = $config;
        }
    }

    public static function config($config)
    {
        self::getInstance()->config($config);
    }

    public static function getInstance()
    {
        $engine = self::$engine_;

        if (!isset($engine)) {
            throw new LogicException();
        }

        static $instance;

        if (!isset($instance)) {
            $engine_class = '\\Turbine\\Web\\Template\\'
                . ucfirst(strtolower($engine)) . 'Template';
            $instance = new $engine_class;

            if (isset(self::$config_)) {
                $instance->config(self::$config_);
            }
        }

        return $instance;
    }
}
?><?php
namespace Turbine\Web\Template;

class SmartyTemplate implements ITemplate
{
    private $smarty_tempalte_;

    public function __construct($config = null)
    {
        $this->smarty_tempalte_ = new \Smarty();

        if (isset($config)) {
            foreach ($config as $key => $value) {
                $this->smarty_tempalte_->{$key} = $value;
            }
        }
    }

    public function assign($key_or_value, $value = null)
    {
        if (is_array($key_or_value)) {
            $this->smarty_tempalte_->assign($key_or_value);
        } else {
            $this->smarty_tempalte_->assign($key_or_value, $value);
        }

        return $this;
    }

    public function fetch($tpl_name, $values = null)
    {
        if (isset($values)) {
            $this->assign($values);
        }

        return $this->smarty_tempalte_->fetch($tpl_name);
    }

    public function display($tpl_name, $values = null)
    {
        if (isset($values)) {
            $this->assign($values);
        }

        return $this;
    }

    public function cache($lifetime)
    {
        $this->smarty_tempalte_->setCacheLifetime($lifetime);

        return $this;
    }

    public function isCached($tpl_name)
    {
        return $this->smarty_tempalte_->isCached($tpl_name);
    }

    public function config($key_or_value, $value = null)
    {
        if (is_array($key_or_value)) {
            foreach ($key_or_value as $key => $value) {
                $this->smarty_tempalte_->{$key} = $value;
            }
        } else {
            $this->smarty_tempalte_->{$key_or_value} = $value;
        }

        return $this;
    }
}
?><?php
namespace Turbine\Web\Template;

interface ITemplate
{
    public function assign($key, $value);

    public function config($key, $value);

    public function fetch($tpl_name);

    public function display($tpl_name);

    public function isCached($tpl_name);

    public function cache($lifetime);
}
?><?php
namespace Turbine\Web\Router;

use Turbine\Core;

class Route
{
    public $rule;
    public $method;
    public $callable;
    public $name;
    public $config;

    public function __construct(
        $rule,
        $method,
        $callable,
        $name = null,
        $config = null)
    {
        $this->rule = $rule;
        $this->method = $method;

        if (is_callable($callable)) {
            $this->callable = $callable;
        } else {
            throw new \InvalidArgumentException();
        }

        $this->name = $name ? $name : null;
        $this->config = new Core\DictBag($config);
    }

    public function call($args)
    {
        return call_user_func($this->callable, $args);
    }

    public function __invoke()
    {
        return call_user_func_array($this->callable, func_get_args());
    }
}
?><?php
namespace Turbine\Web\Router\Filter;

class Filter
{
    public static function create($mode, $conf)
    {
        $name = '\Turbine\Web\Router\Filter\\' . ucfirst($mode) . 'Filter';
        return $name::create($conf);
    }
}

?><?php
namespace Turbine\Web\Router\Filter;

class FloatFilter
{
    private $conf_;

    public static function create($conf)
    {
        return array('-?[\d\.]+', 'floatval', null);
    }
}
?><?php
namespace Turbine\Web\Router\Filter;

class IntFilter
{
    public static function create($conf)
    {
        return array('-?\d+', 'intval', null);
    }
}
?><?php
namespace Turbine\Web\Router\Filter;

class ReFilter
{
    private static $default_pattern_ = '[^/]+';

    public static function create($conf)
    {
        return array($conf ? $conf : self::$default_pattern_, null, null);
    }
}
?><?php
namespace Turbine\Web\Router;

use Turbine\Core\Annotation;
use Turbine\Web\Constant\HttpMethod;

class Routable
{
    public static function __callstatic($method, $arguments)
    {
        $instance = new static();
        $method = substr($method, 4);
        return call_user_func_array(array($instance, $method), $arguments);
    }

    public static function getRoutes()
    {
        $reflection_class = new \ReflectionClass(get_called_class());
        $methods = $reflection_class->getMethods(\ReflectionMethod::IS_PUBLIC);

        $ret = array();

        foreach ($methods as $method) {
            $annotation = new Annotation($method->getDocComment());

            if ($method->isStatic()
                || $method->isConstructor()
                || $method->isDestructor()
            ) {
                continue;
            }

            $name = $method->name;

            if (isset($name[0])
                && isset($name[1])
                && $name[0] == '_'
                && $name[1] == '_'
            ) {
                continue;
            }

            $ret[$name] = array('method' => $name, 'routes' => array());

            $verbs = HttpMethod::getConstants();

            foreach ($verbs as $verb) {
                $verb = strtolower($verb);

                if ($annotation->has($verb)) {
                    $ret[$name]['routes'][] = array(
                        'verb' => $verb,
                        'route' => $annotation->get($verb)
                    );
                }
            }
        }

        return $ret;
    }
}
?><?php
namespace Turbine\Web\Router;

class ArgsMatcher
{
    private $re_;
    private $filters_;

    public function __construct($re, $filters)
    {
        $this->re_ = $re;
        $this->filters_ = $filters;
    }

    public function match($path)
    {
        $ret = preg_match($this->re_, $path, $matches);

        array_shift($matches);

        foreach ($this->filters_ as $key => $filter) {
            $matches[$key] = $filter($matches[$key]);
        }

        return $matches;
    }

    public function __invoke($path)
    {
        return $this->match($path);
    }
}

?><?php
namespace Turbine\Web\Router;

use Turbine\Web\Request\HttpRequest;
use Turbine\Web\Router\Filter\Filter;

class Router
{
    private $rules_ = array();
    private $static_ = array();
    private $dynamic_ = array();

    private $rule_syntax_ = <<<EOF
#(\\\\*)<([a-zA-Z_][\w]*)?(?::([\w]*)(?::((?:\\\\.|[^\\\\>]+)+)?)?)?>#
EOF;

    private $default_filter_ = 're';
    // private $default_pattern_ = '[^/]+';

    const MAX_PATTERN_LEN_ = 65535;

    // private $filters_ = array();

    ///TODO: deal with trailing slash
    private $auto_trailing_slash_ = false;

    public function setAutoTrailing($auto_trailing_slash)
    {
        if (is_null($auto_trailing_slash)) {
            return $this->auto_trailing_slash;
        }

        $this->auto_trailing_slash_ = $auto_trailing_slash;
    }

    public function __construct()
    {
        // $default_pattern = $this->default_pattern_;

        // $this->filters_ = array(
            // 're' =>
            // function($conf) use($default_pattern) {
                // return array(
                    // $conf ? $conf : $default_pattern,
                    // null,
                    // null
                // );
            // },
            // 'int' =>
            // function($conf) {
                // return array(
                    // '-?\d+',
                    // 'intval',
                    // null
                // );
            // },
            // 'float' =>
            // function($conf) {
                // return array(
                    // '-?[\d\.]+',
                    // 'floatval',
                    // null
                // );
            // }
        // );
    }

    public function addFilter($name, $callable)
    {
        // callable must return array(re, func, config)
        $this->filters_[$name] = $callable;
    }

    private function parseRule_($rule)
    {
        $reg_result = preg_match_all(
                            $this->rule_syntax_,
                            $rule,
                            $matches,
                            PREG_SET_ORDER | PREG_OFFSET_CAPTURE
                        );

        $syn_partials = array();
        $prefix = '';
        $offset = 0;

        foreach ($matches as $match) {
            $prefix .= substr($rule, $offset, $match[0][1] - $offset);

            $partial = $match[0][0];
            $partial_len = strlen($partial);
            $partial_begin = $match[0][1];

            if (strlen($match[1][0]) % 2) {
                $prefix .= $partial;
                $offset = $partial_begin + $partial_len;
                continue;
            }

            if ($prefix) {
                $syn_partials[] = array(
                    'key' => $prefix,
                    'filter' => null,
                    'conf' => null
                );
            }

            $syn_partials[] = array(
                'key'
                    => $match[2][0],
                'filter'
                    => isset($match[3]) ? $match[3][0] : $this->default_filter_,
                'conf'
                    => isset($match[4]) ? $match[4][0] : null
            );

            $prefix = '';
            $offset = $match[0][1] + strlen($match[0][0]);
        }

        if ($offset < strlen($rule) || $prefix) {
            $syn_partials[] = array(
                'key' => $prefix . substr($rule, $offset),
                'filter' => null,
                'conf' => null
            );
        }

        return $syn_partials;
    }

    public function add(Route $route)
    {
        $rule = $route->rule;
        $method = $route->method;

        if (in_array($rule, $this->rules_)) {
            $this->rules_[$rule][$method] = $route;
            return;
        }

        $syn_partials = $this->parseRule_($rule);

        $pattern = '';
        $is_static = true;
        $filters = array();
        $this->rules_[$rule] = array($method => $route);
        $target = &$this->rules_[$rule];

        foreach ($syn_partials as $syn_partial) {
            // $key, $mode, $conf
            $key = $syn_partial['key'];
            $mode = $syn_partial['filter'];
            $conf = $syn_partial['conf'];

            if ($mode) {
                $is_static = false;

                // $filter = $this->filters_[$mode]($conf);
                $filter = Filter::create($mode, $conf);

                if ($key) {
                    $pattern .= sprintf('(?P<%s>%s)', $key, $filter[0]);
                    // $pattern .= '(' . $filter[0] . ')';
                } else {
                    $pattern .= sprintf('(?:%s)', $filter[0]);
                }

                if ($filter[1]) {
                    $filters[$key] = $filter[1];
                }
            } elseif ($key) {
                $pattern .= preg_quote($key);
            }
        }

        if ($is_static) {
            $this->static_[$rule] = $target;
            return;
        }

        $re = '#^' . $pattern . '$#';

        // $get_url_args = function($path) use($re, $filters) {
            // $ret = preg_match($re, $path, $url_args);
            // array_shift($url_args);

            // foreach ($filters as $key => $filter) {
                // $url_args[$key] = $filter($url_args[$key]);
            // }

            // return $url_args;
        // };

        $get_url_args = new ArgsMatcher($re, $filters);

        $matchonly_pattern = preg_replace_callback(
                                '#(\\\\*)(\\(\\?P<[^>]*>|\\((?!\\?))#',
                                function($m) {
                                    if (strlen($m[1]) % 2) {
                                        return $m[0];
                                    }

                                    return $m[1] . '(?:';
                                },
                                $pattern
                            );

        $dynamic_end = count($this->dynamic_) - 1;
        $tmp = array($get_url_args, $target);

        if ($dynamic_end < 0) {
            $this->dynamic_[0]
                = array('#(^' . $matchonly_pattern . '$)#', array($tmp));
        } else {
            $combined = rtrim($this->dynamic_[$dynamic_end][0], '#')
                        . '|(^' . $matchonly_pattern . '$)#';

            if (strlen($combined) <= self::MAX_PATTERN_LEN_) {
                $this->dynamic_[$dynamic_end][0] = $combined;
                $this->dynamic_[$dynamic_end][1][] = $tmp;
            } else {
                $this->dynamic_[++ $dynamic_end]
                    = array('#(^' . $matchonly_pattern . '$)#', array($tmp));
            }
        }

        return $get_url_args;
    }

    public function match(HttpRequest $request)
    {
        $path_info = $request->getPathInfo();

        $url_args = array();
        $targets = null;

        if (isset($this->static_[$path_info])) {
            $targets = $this->static_[$path_info];
        } else {
            foreach ($this->dynamic_ as $entry) {
                $combined_re = $entry[0];
                $match_times = preg_match($combined_re, $path_info, $match);

                if (!$match_times) {
                    continue;
                }

                $max_index = count($match) - 2;
                list($get_url_args, $targets) = $entry[1][$max_index];

                if ($get_url_args) {
                    $url_args = $get_url_args($path_info);
                }

                break;
            }
        }

        $method = $request->getMethod();

        if (!$targets) {
            return null;
        } elseif (isset($targets[$method])) {
            return array($targets[$method], $url_args);
        } elseif (HttpMethod::HEAD == $method && isset($targets[$method])) {
            return array($targets[HttpMethod::GET], $url_args);
        } elseif (isset($targets[HttpMethod::ANY])) {
            return array($targets[HttpMethod::ANY], $url_args);
        } else {
            // TODO: for option
        }

        return null;
    }
}
?><?php

class ErrorRouter
{
    public function match()
    {
    }
}
?><?php
namespace Turbine\Web\Event;

class EventData
{
    public $request;
    public $response;
    public $exception;
    public $user_data;
}
?><?php
namespace Turbine\Web\Event;

class Event extends \Turbine\Core\Event
{
    const ON_REQUEST   = 'onrequest';
    const BEFORE_ROUTE = 'beforeroute';
    const AFTER_ROUTE  = 'afterroute';
    const BEFORE_HANDLE_REQUEST = 'beforehandlerequest';
    const AFTER_HANDLE_REQUEST  = 'afterhandlerequest';
}
?><?php
namespace Turbine\Web\Request;

class HttpHeader extends \Turbine\Core\DictBag {
    private $server_;

    private static $key_map_ = array(
        'Accept'            => 'HTTP_ACCEPT',
        'Accept-Charset'    => 'HTTP_ACCEPT_CHARSET',
        'Accept-Encoding'   => 'HTTP_ACCEPT_ENCODING',
        'Accept-Language'   => 'HTTP_ACCEPT_LANGUAGE',
        'Cache-Control'     => 'HTTP_CACHE_CONTROL',
        'Connection'        => 'HTTP_CONNECTION',
        'Content-Length'    => 'CONTENT_LENGTH',
        'Content-Type'      => 'CONTENT_TYPE',
        'Cookie'            => 'HTTP_COOKIE',
        'Host'              => 'HTTP_HOST',
        'User-Agent'        => 'HTTP_USER_AGENT',
        'Referer'           => 'HTTP_REFERER',
        'HTTP_X_REQUESTED_WITH' => 'HTTP_X_REQUESTED_WITH'
    );

    private $http_request_;

    public function __construct($server)
    {
        $this->server_ = $server;
    }

    /**
     * get
     *
     * @return void
     * @author Yuan B.J.
     */
    public function get($key, $value = null)
    {
        if (parent::has($key)) {
            return parent::get($key, $value);
        }

        if (array_key_exists($key, self::$key_map_)) {
            $exact_key = self::$key_map_[$key];
            $ret = $this->server_->get($exact_key);
            parent::set_($exact_key, $value);

            return $ret;
        }

        return $value;
    }
}
?><?php
namespace Turbine\Web\Request;

class HttpHeaders extends \Turbine\Core\DictBag {
    private $server_;

    private static $key_map_ = array(
        'Accept'            => 'HTTP_ACCEPT',
        'Accept-Charset'    => 'HTTP_ACCEPT_CHARSET',
        'Accept-Encoding'   => 'HTTP_ACCEPT_ENCODING',
        'Accept-Language'   => 'HTTP_ACCEPT_LANGUAGE',
        'Cache-Control'     => 'HTTP_CACHE_CONTROL',
        'Connection'        => 'HTTP_CONNECTION',
        'Content-Length'    => 'CONTENT_LENGTH',
        'Content-Type'      => 'CONTENT_TYPE',
        'Cookie'            => 'HTTP_COOKIE',
        'Host'              => 'HTTP_HOST',
        'User-Agent'        => 'HTTP_USER_AGENT',
        'Referer'           => 'HTTP_REFERER',
        'HTTP_X_REQUESTED_WITH' => 'HTTP_X_REQUESTED_WITH'
    );

    private $http_request_;

    public function __construct($server)
    {
        $this->server_ = $server;
    }

    public function get($key, $value = null)
    {
        if (parent::has($key)) {
            return parent::get($key, $value);
        }

        if (array_key_exists($key, self::$key_map_)) {
            $exact_key = self::$key_map_[$key];
            $ret = $this->server_->get($exact_key);
            parent::set_($exact_key, $value);

            return $ret;
        }

        return $value;
    }
}
?><?php
namespace Turbine\Web\Request;

use Turbine\Web\Constant\HttpScheme,
    Turbine\Web\Constant\HttpMethod,
    Turbine\Core\DictBag,
    Turbine\Core\DictBagRO;

class HttpRequest
{
    private $http_host_;

    private $headers_;
    private $session_;
    private $cookie_;

    private $query_data_;
    private $post_data_;
    private $reqeust_data_;
    private $put_data_;
    private $row_put_data_;

    private $http_scheme_;
    private $path_info_;

    private $is_xhr_;
    private $is_flash_request_;

    private $is_delete_;
    private $is_get_;
    private $is_head_;
    private $is_options_;
    private $is_post_;
    private $is_put_;

    public static function getInstance()
    {
        static $instance;

        if (isset($instance)) {
            return $instance;
        }

        return ($instance = new static());
    }

    public function __construct()
    {
        $this->request_data_    = new DictBagRO($_REQUEST);
        $this->query_data_      = new DictBagRO($_GET);
        $this->post_data_       = new DictBagRO($_POST);
        $this->server_          = new DictBagRO($_SERVER);
        $this->cookie_          = new DictBagRO($_COOKIE);
        $this->headers_         = new HttpHeaders($_SERVER);
    }

    public function getHttpHost()
    {
        if (is_null($this->http_host_)) {
            $host = $this->getServer('HTTP_HOST');

            if (!empty($host)) {
                $this->http_host_ = $host;
            } else {
                $scheme = $this->getScheme();
                $name = $this->getServer('SERVER_NAME');
                $port = $this->getServer('SERVER_PORT');

                if (is_null($name)) {
                    $this->http_host_ = '';
                } elseif ((HttpScheme::HTTP == $scheme && 80 == $port)
                            || (HttpScheme::HTTPS == $scheme && 443 == $port)) {
                    $this->http_host_ = $name;
                } else {
                    $this->http_host_ = $name . ':' . $port;
                }
            }
        }

        return $this->http_host_;
    }

    public function getHeader($key = null, $default = null)
    {
        static $special_keys = array(
            'CONTENT_LENGTH',
            'CONTENT_TYPE',
            'PHP_AUTH_USER',
            'PHP_AUTH_PW',
            'PHP_AUTH_DIGEST',
            'AUTH_TYPE'
        );

        if ($key) {
            $key = strtoupper($key);

            if (!in_array($key, $special_keys)) {
                $key = 'HTTP_' . str_replace('-', '_', $key);
            }

            if (!array_key_exists($key, $this->headers_)) {
                return null;
            }

            return $this->headers_[$key];
        }

        switch ($key) {
            case null:
                static $flag = false;

                if ($flag) {
                    return $this->headers_;
                }
        }

        $name = 'HTTP_' . strtoupper(str_replace('-', '_', $key));
        $value = $this->getServer($name, false);
    }

    public function getCookie($key = null, $default = null)
    {
        if (is_null($key)) {
            return $this->cookie_;
        }

        return $this->cookie_->get($key, $default);
    }

    public function getQuery($key = null, $default = null)
    {
        if (is_null($key)) {
            return $this->query_data_;
        }

        return $this->query_data_->get($key, $default);
    }

    public function getPost($key = null, $default = null)
    {
        if (is_null($key)) {
            return $this->post_data_;
        }

        return $this->post_data_->get($key, $default);
    }

    public function getPutRow()
    {
        if (is_null($this->row_put_data_)) {
            $row_put_data = file_get_contents('php://input');

            if (empty($row_put_data)) {
                $this->row_put_data_ = false;
            } else {
                $this->row_put_data_ = $row_put_data;
            }
        }

        return $this->row_put_data_;
    }

    public function getPut($key = null, $default = null)
    {
        if (is_null($this->put_data_)) {
            $row_put_data = $this->getPutRow();

            if ($row_put_data) {
                $this->put_data_ = new DictBag();
            } else {
                mb_parse_str($row_put_data, $output);
                $this->put_data_ = new DictBag($output);
            }
        }

        if (is_null($key)) {
            return $this->put_data_;
        }

        return $this->post_data_->get($key, $default);
    }

    public function getServer($key = null, $default = null)
    {
        if (is_null($key)) {
            return $this->server_;
        }

        return $this->server_->get($key, $default);
    }

    public function getMethod()
    {
        return $this->getServer('REQUEST_METHOD');
    }

    public function isPost()
    {
        if (!isset($this->is_post_)) {
            $this->is_post_
                = (HttpMethod::POST == $this->getServer('REQUEST_METHOD'));
        }

        return $this->is_post_;
    }

    public function isGet()
    {
        if (!isset($this->is_get_)) {
            $this->is_get_
                = (HttpMethod::GET == $this->getServer('REQUEST_METHOD'));
        }

        return $this->is_get_;
    }

    public function isPut()
    {
        if (!isset($this->is_put_)) {
            $this->is_put_
                = (HttpMethod::PUT == $this->getServer('REQUEST_METHOD'));
        }

        return $this->is_put_;
    }

    public function isDelete()
    {
        if (!isset($this->is_delete_)) {
            $this->is_delete_
                = (HttpMethod::DELETE == $this->getServer('REQUEST_METHOD'));
        }

        return $this->is_delete_;
    }

    public function isHead()
    {
        if (!isset($this->is_head_)) {
            $this->is_head_
                = (HttpMethod::HEAD == $this->getServer('REQUEST_METHOD'));
        }

        return $this->is_head_;
    }

    public function isOptions()
    {
        if (!isset($this->is_options_)) {
            $this->is_options_
                = (HttpMethod::OPTION == $this->getServer('REQUEST_METHOD'));
        }

        return $this->is_options_;
    }

    public function isXHR()
    {
        if (!isset($this->is_xhr_)) {
            $this->is_xhr_
                = ('XMLHttpRequest'
                    == strtolower($this->getHeader('X_REQUESTED_WITH')));
        }

        return $this->is_xhr_;
    }

    public function isAjax()
    {
        return $this->isXHR();
    }

    public function isFlashRequest()
    {
        if (!isset($this->is_flash_request_)) {
            $user_agent = strtolower($this->getHeader('User-Agent'));
            $this->is_flash_request_
                = (false === strpos($header, 'flash') ? false : true);
        }

        return $this->is_flash_request_;
    }

    public function getClientIp($check_proxy = true)
    {
        return $ip;
    }

    public function getScheme()
    {
        if (is_null($this->http_scheme_)) {
            $this->http_scheme_ = ($this->getServer('HTTPS') == 'on') ?
                                    HttpScheme::HTTPS : HttpScheme::HTTP;
        }

        return $this->http_scheme_;
    }

    public function isSecure()
    {
        static $is_secure = null;

        if (is_null($is_secure)) {
            $is_secure = ($this->getScheme() == HttpScheme::HTTPS);
        }

        return $is_secure;
    }

    // maybe not compatitable for all kinds of servers
    public function getPathInfo()
    {
        $this->path_info_ = $this->getServer('PATH_INFO');

        if (is_null($this->path_info_)) {
            $query_string = $this->getServer('QUERY_STRING');
            $request_uri = $this->getServer('REQUEST_URI');

            $tmp = null;

            if ($query_string) {
                $qs_pos = strpos($request_uri, $query_string);
                $tmp = substr($request_uri, 0, $qs_pos - 2);
            } else {
                $tmp = $request_uri;
            }

            $this->path_info_ = substr($tmp, strlen($request_uri) + 1);
        }

        return $this->path_info_;
    }

    public function getRequestTime()
    {
        return $this->server_->get('REQUEST_TIME');
    }
}
?>