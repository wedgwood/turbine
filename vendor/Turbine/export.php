<?php
namespace Turbine;

function export($case)
{
    switch ($case)
    {
        case 'web':
            class_alias('Turbine\Web\Turbine', 'Turbine');
            class_alias('Turbine\Web\Event\Event', 'Event');

            class_alias('Turbine\Web\Constant\HttpMethod', 'HttpMethod');
            class_alias('Turbine\Web\Constant\HttpScheme', 'HttpScheme');
            class_alias('Turbine\Web\Constant\HttpStatus', 'HttpStatus');

            class_alias('Turbine\Web\Router\Routable', 'Routable');

            class_alias('Turbine\Cache\Cache', 'Cache');

            class_alias('Turbine\Web\Op\Cookie', 'Cookie');
            class_alias('Turbine\Web\Op\Session', 'Session');
            class_alias('Turbine\Web\Op\Header', 'Header');
            class_alias('Turbine\Web\Op\TemplateEngine', 'TemplateEngine');

            class_alias('Turbine\Compiler\Compiler', 'Compiler');

            class_alias(
                'Turbine\CompilerModule\EventCompilerModule',
                'EventCompilerModule'
            );

            class_alias(
                'Turbine\CompilerModule\CacheCompilerModule',
                'CacheCompilerModule'
            );

            class_alias(
                'Turbine\Web\CompilerModule\RouteCompilerModule',
                'RouteCompilerModule'
            );

            class_alias(
                'Turbine\Web\CompilerModule\ErrorRendererCompilerModule',
                'ErrorRendererCompilerModule'
            );

            require __DIR__ . '/Web/Op/response.php';
            require __DIR__ . '/Web/Op/route.php';
            require __DIR__ . '/Web/Op/template.php';
            require __DIR__ . '/Web/Op/event.php';

            break;
        case 'cli':
            // TODO
    }

    class_alias('Turbine\Core\Configure', 'Configure');
}
