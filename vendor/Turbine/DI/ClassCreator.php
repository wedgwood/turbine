<?php
namespace Turbine\DI;

class ClassCreator
{
    private $class_;

    public function __construct($class)
    {
        $this->class_ = new \ReflectionClass($class);
    }

    public function __invoke()
    {
        return $this->class_->newInstanceArgs(func_get_args());
    }
}
