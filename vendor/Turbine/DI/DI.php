<?php
namespace Turbine\DI;

class DI
{
    private $values_ = array();
    private $shared_ = array();

    public function set($key, $value)
    {
        $args = func_num_args() > 2 ? array_slice(func_get_args(), 2) : array();

        $this->values_[$key] = array(
            is_callable($value) ? $value : new ClassCreator($value),
            $args
        );

        return $this;
    }

    public function get($key)
    {
        $args = func_num_args() > 1 ? array_slice(func_get_args(), 1) : array();
        return call_user_func_array(
            $this->values_[$key][0],
            $this->values_[$key][1]
        );
    }

    public function remove($key)
    {
        unset($this->values_[$key]);
        return $this;
    }

    public function getShared($key)
    {
        if (!isset($this->shared_[$key])) {
            $this->shared_[$key] = call_user_func_array(
                array($this, 'get'),
                func_get_args()
            );
        }

        return $this->shared_[$key];
    }

    public function removeShared($key)
    {
        unset($this->shared_[$key]);
        return $this;
    }
}
