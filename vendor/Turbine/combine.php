<?php

$files = array();

function fillFiles($dir)
{
    $dh = opendir($dir);

    if (!$dh) {
        exit('error');
    }

    global $files;

    while ($file = readdir($dh)) {
        if ($file[0] == '.') {
            continue;
        }

        $fullpath = $dir . '/' . $file;

        if (is_dir($fullpath)) {
            fillFiles($fullpath);
            continue;
        }

        if (substr($fullpath, -4) == '.php') {
            $files[] = $fullpath;
        }
    }
}

fillFiles(__DIR__ . '/Core/');
fillFiles(__DIR__ . '/CompilerModule/');
fillFiles(__DIR__ . '/Compiler/');
fillFiles(__DIR__ . '/Cache/');
fillFiles(__DIR__ . '/Web/');
fillFiles(__DIR__ . '/Op/');

$content = '';

foreach ($files as $file) {
    $file_content = file_get_contents($file);
    $tmp = '<?php' . preg_replace('/\<\?php|\?>\s*$/i', '', $file_content) . '?>';
    $content .= $tmp;
}

echo $content;
